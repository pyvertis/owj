Open Water Journal Django Website  
==================  

####Vertis Testing Server####
URL: http://owj.pydevs.com  
You are required to set the host be able to access this url.  
````  
10.10.10.44  owj.pydevs.com  
````  

####Staging Server####
Staging server is hosted on Digital Ocean  
Below is the server access informtion  

		Public Network
		IP Address: 104.131.204.46
		Gateway: 104.131.192.1
		Netmask: 255.255.224.0
  
Root User (root, ikYZAUJIL)  
Byu User (byu, LIh4kqjE)  

#####Website can be accessed at http://104.131.204.46/ #####

####Configuring new server####

User should have application code on his machine. There is fabric script that helps in setting up new server.  
  
+ On your machine cd into owj/owj dir (dir in which fabfile is located).  
+ Create a new settings file in owj/owj/owj/settings folder by the name local_{hostname}.py .
Create this file duplicating local_pydevs.py . Update the settings according to the machine.  
+ Update the owj/owj/settings/__init__ file to load your new settings aswell.  
+ Create a wsgi file fot the machine in folder owj/owj/apache.Name should be something, 
which allow easy identification. ie: {staging}wsgi.py .
+ Change the configuration in wsgi file according to your machine.  
+ Create a apache virtualhost file in owj/owj/apache/virtualhost folder. Create it duplicating pydevs.  
+ Edit the file to suit your configuration. Make sure you update the wsgi file name to the new wsgi file that you created.  
+ Commit the code changes to master branch and push to repository.  
+ Edit the fabric file to create a new function like pydevs function. Set the values for the new machine.  
+ Create new sudo user. if you already have a sudo user, you can skip this.  
 run below command:  

		fab create_user -H hostname -u rootuser -p rootpassword

	Where rootuser should be a user who has permissions to create new user.  
+ Edit the new config function that you created to use the newly created user and password. 
+ Create ssh key for new user. Or if you already have ssh key skip this step.      
		
		fab create_ssh_key -H {configfunctionname}  
		  
+ Save the generated key or the key that you already have in repository access keys, so that machine can access the repository.  
+ Prepare the machine for application installation.    
		
		fab bootstrap -H  {configfunctionname}  
		
+ This should complete the installation of the system. if something fails you will have to deal with it manually.  
+ Next we need to load the initial fixtures.  
		
		fab load_initialdata -H {configfunctionname}  
		
+ Try to access the application.  


  
####Deploying continous builds on pydevs.com  
+ update the required branch on stash.
+ run below command:

		fab build_install:{branchname} -H pydevs  
		fab load_initialdata -H pydevs  
		
		
		
