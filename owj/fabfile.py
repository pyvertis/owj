from fabric.api import env, cd, abort, sudo, run, prefix, local, task
from fabric.context_managers import lcd
from fabric.contrib import django
from fabric.operations import prompt
from fabutil import select_db_type
from fabutil.utils import add_user
import os
import sys


path = os.path.abspath(os.path.join(os.path.dirname(__file__), "owj"))
sys.path.insert(0, path)


RSYNC_EXCLUDE = (
    '.DS_Store',
    '.hg',
    '*.pyc',
    '*.example',
    '*.db',
    'media/admin',
    'media/attachments',
    'local_settings.py',
    # 'fabfile.py',
    # 'bootstrap.py',
)

env.project = 'owj'
env.git_repo = "ssh://git@10.10.10.80:7999/OWJ/owj.git"

APTGET_PACKAGES = [ "pkg-config",
        "postgresql", "libpq-dev", "vim", "libreadline6",
        "libreadline6-dev", "python-dev", "python-distribute",
        "libjpeg8", "libjpeg-dev", "libfreetype6", "libfreetype6-dev", "zlib1g-dev",
        "git", "apache2", "libapache2-mod-wsgi", "graphviz", "libgraphviz-dev",
        "python-pip", "python-virtualenv"
]


def _setup_path():
    env.root = os.path.join(env.home, 'www', env.environment)
    env.code_root = os.path.join(env.root, env.project)
    env.virtualenv_root = os.path.join(env.root, 'env')
    env.settings = '%(project)s.settings_%(environment)s' % env


def arun_machine():
    """
    use local dev's env
    """
    env.user = "arun"
    env.environment = 'local'
    env.hosts = ['127.0.0.0']
    env.host_string = "localhost"
    env.home = "/home/arun/"
    env.root = "/home/arun/workspace/"
    env.code_root = os.path.join(env.root, env.project)
    env.virtualenv_root = os.path.join("/home/arun/pyenvs", env.project)
    env.djsettings = 'owj.settings.__init__'
    env.wsgi = "stagingwsgi.py"
    env.dbuser="postgres"
    env.dbpassword= 'postgres'


def harshal_machine():
    """
    use local dev's env
    """
    env.user = "vertis"
    env.environment = 'local'
    env.hosts = ['127.0.0.0']
    env.host_string = "localhost"
    env.home = "/home/vertis/"
    env.root = "/home/vertis/pyapps"
    env.code_root = os.path.join(env.root, env.project)
    env.virtualenv_root = os.path.join(env.code_root, "venv")
    env.djsettings = 'owj.settings.__init__'
    env.wsgi = "stagingwsgi.py"

def staging():
    """ use staging environment on remote host"""
    env.user = 'byu'
    env.environment = 'staging'
    env.hosts = ['104.131.204.46']
    env.password = "LIh4kqjE"
    env.host_string = "104.131.204.46"
    env.home = "/home/{}/".format(env.user)
    env.root = "/home/{}/pyapps/".format(env.user)
    env.code_root = os.path.join(env.root, env.project)
    env.virtualenv_root = os.path.join(env.code_root, "venv")
    env.djsettings = 'owj.settings.__init__'
    env.wsgi = "stagingwsgi.py"
    env.git_repo = "git@bitbucket.org:pyvertis/owj.git"
    env.dbuser="owj"
    env.dbpassword= 'owj123456'


def pydevs():
    """ use pydevs testing environment on remote host"""
    env.user = 'vertis'
    env.environment = 'pydevs'
    env.hosts = ['10.10.10.44']
    env.password = "reverse"
    env.host_string = "pydevs.com"
    env.home = "/home/vertis/"
    env.root = "/home/vertis/pyapps/"
    env.code_root = os.path.join(env.root, env.project)
    env.virtualenv_root = os.path.join(env.code_root, ".venv")
    env.djsettings = 'owj.settings.__init__'
    env.wsgi = "pydevswsgi.py"
    env.dbuser="postgres"
    env.dbpassword= 'postgres'


def setup_env(original_function):
    """
    decorator to set env variables based on the host
    """
    def new_function(*args, **kwargs):
        globals()[env.host]()
        django.settings_module(env.djsettings)
        original_function(*args, **kwargs)  # the () after "original_function" causes original_function to be called
    return new_function


venv = lambda : "source {}".format(os.path.join(env.virtualenv_root, "bin/activate"))
djapp = lambda : os.path.join(env.code_root, "owj")


@setup_env
def setup_database():
    """
    Create Database and database user, using django settings
    """
    def _validate_user_input(value):
        if value not in ["Y", "y", "N", "n"]:
            raise Exception("Not a valid input. Available option: Y/N")
        else:
            return value.upper()

    db_type_class = select_db_type()
    if db_type_class:
        db = db_type_class()
        db_password = db.install()
        from django.conf import settings
        dbname = settings.DATABASES['default']["NAME"]
        password = env.dbpassword
        user = env.dbuser
        feedback = prompt("Do you want to create a Database user: {} with password: {}. [Y/N]".format(user, password),
                          default="N", validate=_validate_user_input)
        if feedback == "Y":
            db.create_db_and_user(dbname, user, password)
        else:
            db.create_db(dbname)
        db.grant_privileges(dbname, user)


def _build_machine():
    """
    """
    sudo("apt-get update")
    sudo("apt-get install %s" % " ".join(APTGET_PACKAGES))


def create_user():
    """
    Creates a new System User on the Given host
    """
    def _validate(value):
        if not value:
            raise Exception("Empty value not allowed.")
        return value

    print "Create a New User"
    username = prompt("Username: ", validate=_validate)
    password = prompt("password: ", validate=_validate)
    add_user(username, password, True)


def setup_machine():
    """
    Make the machine ready for installing the application
    """
    _build_machine()
    run('mkdir -p %s' % env.root)
#     sudo("chsh -s /bin/user {}".format(env.user))


def production():
    """ use production environment on remote host"""
    utils.abort('Production deployment not yet implemented.')


@setup_env
@task
def bootstrap():
    """ initialize remote host environment (virtualenv, deploy, update) """
#     require('root', provided_by=('staging', 'production', 'pydevs', 'local'))
    setup_machine()
    init_deploy()
    create_virtualenv()
    update_requirements()
    setup_database()
    apache_config()
    build_install()
    apache_reload()


@setup_env
def apache_config():
    """
    """
    apache_site_dir = "/etc/apache2/sites-available"
    virtualhost_file_in_app = os.path.join(djapp(), "apache/virtualhost", env.environment)
    virtualhostfilename = "{}.conf".format(env.project)
    apachevhostfilepath = os.path.join(apache_site_dir, virtualhostfilename)
    with cd(apache_site_dir):
        sudo("ln -s {} {}".format(virtualhost_file_in_app, apachevhostfilepath))
        sudo("a2ensite {}".format(virtualhostfilename))
    apache_reload()


def sync_data():
    """
    """
    with cd(djapp()):
        with prefix("source {}".format(os.path.join(env.virtualenv_root, "bin/activate"))):
            run("python manage.py syncdb")
            run("python manage.py migrate main")
            run("python manage.py migrate --all")
            run("python manage.py collectstatic  --noinput")
            run("python manage.py compress --force")


@setup_env
def build_install(branch="master"):
    """
    Uploads a new build for django project
    """
#     _build_machine()
    with cd(env.code_root):
        run("git checkout {} && git pull origin {}".format(branch, branch))
        update_requirements()
        sync_data()
        touch()


def create_virtualenv():
    """ setup virtualenv on remote host """
    args = '--clear --distribute'
    run('virtualenv %s %s' % (args, env.virtualenv_root))


def init_deploy():
    """
    Deploy code from repo for the first time
    """
    with cd(env.root):
        run("git clone %s %s" % (env.git_repo, env.code_root))


def update_requirements():
    """ update external dependencies on remote host """
    with cd(env.code_root):
        with prefix("source {}".format(os.path.join(env.virtualenv_root, "bin/activate"))):
            run("pip install -r requirement.txt")


def touch():
    """ touch wsgi file to trigger reload """
    apache_dir = os.path.join(djapp(), 'apache')
    if not env.environment == 'local':
        with cd(apache_dir):
            run('touch {}'.format(env.wsgi))


def update_apache_conf():
    """ upload apache configuration to remote host """
    require('root', provided_by=('staging', 'production'))
    source = os.path.join('apache', '%(environment)s.conf' % env)
    dest = os.path.join(env.home, 'apache.conf.d')
    put(source, dest, mode=0755)
    apache_reload()


def configtest():
    """ test Apache configuration """
    require('root', provided_by=('staging', 'production'))
    run('apache2ctl configtest')


@setup_env
def apache_reload():
    """ reload Apache on remote host """
    sudo('sudo /etc/init.d/apache2 reload')


def apache_restart():
    """ restart Apache on remote host """
    require('root', provided_by=('staging', 'production'))
    sudo('sudo /etc/init.d/apache2 restart')


def symlink_django():
    """ create symbolic link so Apache can serve django admin media """
    require('root', provided_by=('staging', 'production'))
    admin_media = os.path.join(env.virtualenv_root,
                               'src/django/django/contrib/admin/media/')
    media = os.path.join(env.code_root, 'media/admin')
    if not files.exists(media):
        run('ln -s %s %s' % (admin_media, media))


def reset_local_media():
    """ Reset local media from remote host """
#     require('root', provided_by=('staging', 'production'))
    media = os.path.join(env.code_root, 'media', 'upload')
    local('rsync -rvaz %s@%s:%s media/' % (env.user, env.hosts[0], media))


@setup_env
def build_local():
    """
    build local project on  the existing code, without changing branch
    """
#     _build_machine()
    with cd(env.code_root):
        update_requirements()
        sync_data()


@setup_env
def dump_initialdata():
    app_labels = ["contenttypes", "sites", "cms", "djangocms_text_ckeditor"]
    with cd(djapp()):
        with prefix(venv()):
            run("python manage.py dumpdata {} --format xml --indent 4 > fixtures/initial_data.xml".format(" ".join(app_labels)))


@setup_env
def load_initialdata():
    data_files = ["fixtures/initial/contenttype.xml", "fixtures/initial/auth.xml", ]
    # "fixtures/initial/main.xml"
    with cd(djapp()):
        with prefix(venv()):
            for i in data_files:
                run("python manage.py loaddata {}".format(i))


@setup_env
def create_ssh_key():
    """
    Create new ssh key for the machine
    """
    run("ssh-keygen")
    print "New ssh key: "
    run("cat {}".format(os.path.join(env.home, ".ssh/id_rsa.pub")))


@setup_env
def dumpdata(filename):
    """
    create a dump of database in sql format
    """
    sudo("pg_dump owj -U postgres -f {}".format(os.path.join(env.code_root, "db", filename)))


@setup_env
def loaddata(filepath):
    """
    loads the sql data from the given file.
    absolute filepath should be given
    """
    sudo("psql owj -U postgres < {}".format(filepath))
    

@setup_env
def create_cms_fixture():
    """
    creates the initial fixture file for cms
    """
    apps = ["cms", "mptt", "menus", "sekizai", 'djangocms_snippet',
    'djangocms_text_ckeditor',
    'djangocms_picture',]
    with cd(djapp()):
        with prefix(venv()):
            run("python manage.py dumpdata --format xml --indent 4 {apps} > fixtures/initial/cms.xml".format(
                          apps=" ".join(apps)  ))


@setup_env
def django_maange(cmd):
    with cd(djapp()):
        with prefix(venv()):
            run("python manage.py {}".format(cmd))
