#main settings file. base.py will have all the common settings 
#machine specific settings will be loaded in local_{hostname} file

import socket
from django.core import exceptions

hostname = socket.gethostname().replace("-", "")

from base import *

if hostname == "arunInspiron3521":
    from local_arunInspiron3521 import *
elif hostname == "harshalubuntu":
    from local_harshalubuntu import *
elif hostname == "pydevs":
    from local_pydevs import *
elif hostname == "openWaterStag":
    from local_openWaterStag import *
else:
    raise exceptions.ImproperlyConfigured("Local settings for host:{} not found".format(hostname))