"""
Django settings for owj project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
DJANGO_DIR = os.path.abspath(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))

PROJECT_PATH = os.path.abspath(os.path.dirname(DJANGO_DIR))
# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '1+drprsr83frq#qw^zpca)9d1p(hda1q$v5)5hx7497p25jmuz'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ["owj.staging.com", "owj.pydevs.com"]

SITE_ID = 1

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.humanize',
    'compressor',
    # cms
    'cms',  # django CMS itself
    'mptt',  # utilities for implementing a modified pre-order traversal tree
    'menus',  # helper for model independent hierarchical website navigation
    'south',  # intelligent schema and data migrations
    'sekizai',  # for javascript and css management
    'djangocms_admin_style',  # for the admin skin. You **must** add 'djangocms_admin_style' in the list **before** 'django.contrib.admin'.

    # cms-plugins
    'djangocms_snippet',
    'djangocms_text_ckeditor',
    'djangocms_picture',

    # third party apps
    'django_extensions',
    'registration',
    'filer',
    'easy_thumbnails',
    'taggit',
    'sortedm2m',
    'rest_framework',
    'django_js_reverse',

    # build apps
    'main',
    'articles',
    'reg',
    'dbmail',
    'configsettings'


)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',

    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',

)


TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.i18n',
    'django.core.context_processors.request',
    'django.core.context_processors.debug',
    'django.core.context_processors.media',
    'django.core.context_processors.static',

    'django.contrib.messages.context_processors.messages',
    'sekizai.context_processors.sekizai',
    'cms.context_processors.cms_settings',
    'owj.context_processors.disqus'

)

# adding languages for django-cms
LANGUAGES = [
    ('en-us', 'English'),
]


ROOT_URLCONF = 'owj.urls'

# WSGI_APPLICATION = 'apache.wsgi.application'

STATIC_ROOT = os.path.join(PROJECT_PATH, "collected_static")

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.abspath(os.path.join(DJANGO_DIR, "static")),
)


TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(DJANGO_DIR, "templates"),
    # os.path.join(PROJECT_PATH, "templates", "cms", "plugins"),
)

# adding cms templates for django-cms
CMS_TEMPLATES = (
    ('_layout/cmsbase.html', 'Cms Base'),
    ('_layout/left_sub_nav_base.html', 'Left Subnav Layout'),
    ('_layout/cms_author_base.html', 'Author'),
    ('_layout/cms_reviewer_base.html', 'Reviewer'),
)

AUTH_USER_MODEL = 'main.User'

MEDIA_ROOT = os.path.abspath(os.path.join(PROJECT_PATH, "media"))
MEDIA_URL = "/media/"


AUTHENTICATION_BACKENDS = ('reg.emailauthbackend.EmailAuthBackend',)

ACCOUNT_ACTIVATION_DAYS = 14
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = '587'
EMAIL_HOST_USER = 'noreply@vertisinfotech.com'
EMAIL_HOST_PASSWORD = 'Smtp!2#4'
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'noreply@vertisinfotech.com'



OWJ_ADMIN_GROUPS = ["Admin",
              "Editor"
              ]

OWJ_DEFAULT_GROUP = "Author"

OWJ_PUBLIC_REGISTRATION_GROUPS = ["Reviewer",
                                "Author"
                                    ]
OWJ_GROUPS = OWJ_ADMIN_GROUPS + OWJ_PUBLIC_REGISTRATION_GROUPS

######## Django Filer seetings ##################
FILER_IS_PUBLIC_DEFAULT = False

######################################
SOUTH_MIGRATION_MODULES = {
  'taggit': 'taggit.south_migrations',
}

FIXTURE_DIRS = (os.path.abspath(os.path.join(DJANGO_DIR, "fixtures")),)


########## LOGGING CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#logging
# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console':{
            'level': 'DEBUG',
#             'class': 'logging.StreamHandler',
                'class': 'owj.logger.ColorizingStreamHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'owj': {
            'handlers': ['console', 'mail_admins'],
            'level': 'DEBUG'
        }
    }
}
########## END LOGGING CONFIGURATION


########## MANAGER CONFIGURATION
# developers are requested to copy below settings in their own settings and modified as required.
# See: https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = (
    ('Arun', 'arun@vertisinfotech.com'),
)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = (
    ('Arun', 'arun@vertisinfotech.com'),
)
########## END MANAGER CONFIGURATION

INTERNAL_IPS = ("127.0.0.1",)

# payapal configurations #

PUBLISH_ARTICLE_COST = 1  # USD


PAYPAL_MODE = 'sandbox'  # change this to live for production environment
PAYPAL_CURRENCY_CODE = 'USD'
PAYPAL_API_USERNAME = 'harshal-facilitator_api1.vertisinfotech.com'
PAYPAL_API_PASSWORD = '1364887198'
PAYPAL_API_SIGNATURE = 'An5ns1Kso7MWUdW4ErQKJJJ4qi4-AcL5cscdh5tas.ayzvH9xdA71R5k'
PAYPAL_API_VERSION = 117  # see https://developer.paypal.com/docs/classic/release-notes/merchant/PayPal_Merchant_API_Release_Notes_117/
PAYPAL_PAYMENT_REQUEST = 'Sale'


PAYPAL_DEFAULTS = {
    'USER': PAYPAL_API_USERNAME,
    'PWD': PAYPAL_API_PASSWORD,
    'SIGNATURE': PAYPAL_API_SIGNATURE,
    'VERSION': PAYPAL_API_VERSION,
    'CURRENCYCODE':PAYPAL_CURRENCY_CODE,
    'PAYMENTREQUEST_0_CURRENCYCODE':PAYPAL_CURRENCY_CODE,
    'ALLOWNOTE':1
}

INTERNAL_IPS = ("127.0.0.1",)

DISQUS = {
    "shortname": "owjdev"
}


STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # for django compressor
    'compressor.finders.CompressorFinder',
)

# enable django Compressor. This should be kept enabled in testing, staging and Production envs
COMPRESS_ENABLED = True
COMPRESS_STORAGE = 'compressor.storage.GzipCompressorFileStorage'
COMPRESS_OFFLINE = True
COMPRESS_CSS_FILTERS = ['compressor.filters.css_default.CssAbsoluteFilter',
                        'compressor.filters.cssmin.CSSMinFilter'
                        ]