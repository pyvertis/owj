from base import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',  # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'owj2',  # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'localhost',  # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '5432',  # Set to empty string for default.
    }
}

# only for my local development
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


########## MANAGER CONFIGURATION
# developers are requested to copy below settings in their own settings and modified as required.
# See: https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = (
    ('Arun', 'arun@vertisinfotech.com'),
)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = (
    ('Arun', 'arun@vertisinfotech.com'),
)
########## END MANAGER CONFIGURATION

DEBUG = True

# enable django Compressor
COMPRESS_ENABLED = True

ALLOWED_HOSTS = ["localhost"]
