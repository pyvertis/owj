from django.conf.urls import patterns, include, url
from main.api.views import ArticleViewSet, AuthorViewSet, ReviewerViewSet, \
    ReviewerReviewsViewSet, EditorReviewsViewSet, EditorViewSet, ReviewsViewSet, \
    ListArticleReviewers, VolumeViewSet, IssueViewSet, CurrentUserView, \
    StatusListView, ReviewerStatusListView, DateListView, UsersListView, \
    ListVolIssuesView, InviteUserView, IssueStatusListView,\
    ListMostPopularArticles

from rest_framework import routers

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'author/articles', AuthorViewSet, "author")
router.register(r'reviewer/articles', ReviewerViewSet, "reviewer")
router.register(r'editor/articles', EditorViewSet, "editor")
router.register(r'articles', ArticleViewSet)
router.register(r'reviewers/reviews', ReviewerReviewsViewSet)
router.register(r'editors/reviews', EditorReviewsViewSet)
router.register(r'reviews', ReviewsViewSet)
router.register(r'volumes', VolumeViewSet)
router.register(r'issues', IssueViewSet)



urlpatterns = patterns('',
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    url(r"^", include(router.urls)),
    url(r"^me/$", CurrentUserView.as_view()),
    url(r"^statusList/$", StatusListView.as_view()),
    url(r"^reviewerStatusList/$", ReviewerStatusListView.as_view()),
    url(r"^issueStatusList/$", IssueStatusListView.as_view()),

    url(r"^dateList/$", DateListView.as_view()),

    url(r"^article/(?P<id>[0-9]+)/reviewers/$", ListArticleReviewers.as_view()),
    url(r"^users/$", UsersListView.as_view()),

    url(r"^volumes/(?P<id>[0-9]+)/issues/$", ListVolIssuesView.as_view()),
    url(r"^invitedusers/$", InviteUserView.as_view()),
    url(r"^popular/articles/$", ListMostPopularArticles.as_view())


)
