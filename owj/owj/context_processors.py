from django.conf import settings
import logging
from django.core.exceptions import ImproperlyConfigured
logger = logging.getLogger("owj")


def disqus(request):
    """
    returns disqus settings to be used in templates
    """
    try:
        disqus_settings = settings.DISQUS
        return {"disqus": disqus_settings}
    except AttributeError:
        logger.error("Disqus settings are not configured!")
        raise ImproperlyConfigured("Disqus settings are missing or ill configured!")
        
        

    
