from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.decorators.cache import cache_page
from django_js_reverse.views import urls_js
from owj.admin import admin_site

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^developer/', include(admin.site.urls)),
    url(r'^admin/', include(admin_site.urls)),
    url(r'^api/v1/', include('owj.apiurls')),
    url(r'^jsreverse/$', cache_page(3600)(urls_js), name='js_reverse'),
    
    url(r'^', include('main.urls')),
    url(r'^', include('reg.urls')),
    url(r'^', include('articles.urls')),

    url(r'^', include('cms.urls')),
)

if settings.DEBUG:
    urlpatterns = patterns('',
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    url(r'', include('django.contrib.staticfiles.urls')),
) + urlpatterns
