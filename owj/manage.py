#!/usr/bin/env python
import os
import sys , socket

hostname = socket.gethostname()

if __name__ == "__main__":
    
#     os.environ.setdefault("DJANGO_SETTINGS_MODULE", "owj.settings.local_{}".format(hostname.replace("-", "")))
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "owj.settings.__init__")
#     print os.environ["OWJ_SETTINGS_MODULE"]
#     os.environ.setdefault("DJANGO_SETTINGS_MODULE", os.environ["OWJ_SETTINGS_MODULE"])

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
