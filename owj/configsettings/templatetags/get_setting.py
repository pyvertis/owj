from django import template
from configsettings import get

register = template.Library()

@register.simple_tag
def get_setting(key):
    return get(key)
