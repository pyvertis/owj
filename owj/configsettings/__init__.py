from configsettings.models import Settings
from configsettings.utils import utils

@utils.function_cache(3600)
def get(key):
    setting = Settings.objects.filter(key=key)
    if len(setting):
        return setting[0].value
    return None

__all__ = ['get']
