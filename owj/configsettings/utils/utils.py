from Crypto.Hash import SHA224
from django.core.cache import cache

def function_cache(seconds=0):
    """
    A `seconds` value of `0` means that we will not memcache it.
    
    If a result is in memcached, return that first.  If that fails, 
    hit the db and cache in memcache. 
    
    ** NOTE: function that return None are always "recached".
    """

    def inner_cache(method):

        def x(*args, **kwargs):
            key = SHA224.new(str(method.__module__) + str(method.__name__) + \
                  str(args) + str(kwargs)).hexdigest()

            result = cache.get(key)

            if result is None:
                # cache failed, call the actual method
                result = method(*args, **kwargs)

                # save to memcache
                if seconds and isinstance(seconds, int):
                    cache.set(key, result, seconds)

            return result

        return x

    return inner_cache
