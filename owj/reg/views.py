from django.contrib.auth.views import login
from django.shortcuts import redirect
from django.core.urlresolvers import reverse
from django.views.generic.base import TemplateView


class OwjLogin(TemplateView):
    '''
    login mechanism based on userID as email and password 
    if user is already logged in user will be directed to dashboard
    '''
    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return login(request, *args, **kwargs)
        else:
            return redirect(reverse("home"))

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return login(request, *args, **kwargs)
        else:
            return redirect(reverse("home"))
