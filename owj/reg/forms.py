from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model
from main.models import Role
from django.conf import settings
User = get_user_model()

class OwjRegistrationForm(forms.Form):
    """
    Form for registering a new user account.
    
    Validates that the requested username is not already in use, and
    requires the password to be entered twice to catch typos.
    
    Subclasses should feel free to add any additional validation they
    need, but should avoid defining a ``save()`` method -- the actual
    saving of collected user data is delegated to the active
    registration backend.

    """
    required_css_class = 'required'

    email = forms.EmailField(widget=forms.TextInput(attrs={"placeholder":"Your Email", "class":"form-control"}),
                             label=_("Your Email"))
    password = forms.CharField(widget=forms.PasswordInput(attrs={"placeholder":"Password - Min 6 characters", "class":"form-control"}),
                                label=_("Password"), min_length=6)
    confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={"placeholder":"Confirm Password", "class":"form-control"}),
                                label=_("Confirm Password"))


    role = forms.ModelChoiceField(widget=forms.Select(attrs={"placeholder":"Register as", "class":"form-control"}),
                             queryset=Role.objects.filter(name__in=settings.OWJ_PUBLIC_REGISTRATION_GROUPS),
                                label=_("Register as"))
    first_name = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"First Name", "class":"form-control"}),
                                 label=_("First Name"), max_length="30")
    last_name = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"Last Name", "class":"form-control"}),
                                label=_("Last Name"), max_length="30")
    affiliation = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"Affiliation", "class":"form-control", "rows":"5"}), label=_("Affiliation"))
    about_me = forms.CharField(widget=forms.Textarea(attrs={"placeholder":"About Yourself", "class":"form-control", "rows":"5"}), label=_("About Yourself"))

    def clean_role(self):

        role = Role.objects.get(id=self.cleaned_data['role'].id)
        if role.name not in settings.OWJ_PUBLIC_REGISTRATION_GROUPS:
            raise forms.ValidationError(_("Role not allowed."))

        return self.cleaned_data['role']

    def clean_email(self):

        existing = User.objects.filter(email__iexact=self.cleaned_data['email'])
        if existing.exists():
            raise forms.ValidationError(_("A user with same email already exists."))
        else:
            return self.cleaned_data['email']

    def clean(self):
        """
        Verifiy that the values entered into the two password fields
        match. Note that an error here will end up in
        ``non_field_errors()`` because it doesn't apply to a single
        field.
        
        """
        if 'password' in self.cleaned_data and 'confirm_password' in self.cleaned_data:
            if self.cleaned_data['password'] != self.cleaned_data['confirm_password']:
                raise forms.ValidationError(_("The two password fields didn't match."))
        return self.cleaned_data
