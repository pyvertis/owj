from django.conf import settings
from django.contrib.sites.models import RequestSite
from django.contrib.sites.models import Site

from registration import signals
from registration.views import ActivationView as BaseActivationView
from registration.views import RegistrationView as BaseRegistrationView
from django.contrib.auth import get_user_model


from random import choice
from string import ascii_lowercase, digits
from reg.forms import OwjRegistrationForm
from reg.models import OwjRegistrationProfile
from django.shortcuts import redirect
from django.core.urlresolvers import reverse
import logging
from django.contrib.auth.models import Group
from registration.backends.default.views import ActivationView
from dbmail.mail import DbSendMail
import os

logger = logging.getLogger("owj")

User = get_user_model()

class RegistrationView(BaseRegistrationView):
    """
    A registration backend which follows a simple workflow:

    1. User signs up, inactive account is created.

    2. Email is sent to user with activation link.

    3. User clicks activation link, account is now active.

    Using this backend requires that

    * ``registration`` be listed in the ``INSTALLED_APPS`` setting
      (since this backend makes use of models defined in this
      application).

    * The setting ``ACCOUNT_ACTIVATION_DAYS`` be supplied, specifying
      (as an integer) the number of days from registration during
      which a user may activate their account (after that period
      expires, activation will be disallowed).

    * The creation of the templates
      ``registration/activation_email_subject.txt`` and
      ``registration/activation_email.txt``, which will be used for
      the activation email. See the notes for this backends
      ``register`` method for details regarding these templates.

    Additionally, registration can be temporarily closed by adding the
    setting ``REGISTRATION_OPEN`` and setting it to
    ``False``. Omitting this setting, or setting it to ``True``, will
    be interpreted as meaning that registration is currently open and
    permitted.

    Internally, this is accomplished via storing an activation key in
    an instance of ``registration.models.RegistrationProfile``. See
    that model and its custom manager for full documentation of its
    fields and supported operations.
    
    """
    form_class = OwjRegistrationForm

    def get(self, request, *args, **kwargs):
        # Pass request to get_form_class and get_form for per-request
        # form control.
        if request.user.is_authenticated():
            return redirect(reverse("home"))

        form_class = self.get_form_class(request)
        form = self.get_form(form_class)
        return self.render_to_response(self.get_context_data(form=form))

    def post(self, request, *args, **kwargs):
        # Pass request to get_form_class and get_form for per-request
        # form control.
        if request.user.is_authenticated():
            return redirect(reverse("home"))

        form_class = self.get_form_class(request)
        form = self.get_form(form_class)
        if form.is_valid():
            # Pass request to form_valid.
            return self.form_valid(request, form)
        else:
            return self.form_invalid(form)

    def register(self, request, **cleaned_data):
        """
        Given a username, email address and password, register a new
        user account, which will initially be inactive.

        Along with the new ``User`` object, a new
        ``registration.models.RegistrationProfile`` will be created,
        tied to that ``User``, containing the activation key which
        will be used for this account.

        An email will be sent to the supplied email address; this
        email should contain an activation link. The email will be
        rendered using two templates. See the documentation for
        ``RegistrationProfile.send_activation_email()`` for
        information about these templates and the contexts provided to
        them.

        After the ``User`` and ``RegistrationProfile`` are created and
        the activation email is sent, the signal
        ``registration.signals.user_registered`` will be sent, with
        the new ``User`` as the keyword argument ``user`` and the
        class of this backend as the sender.

        """
        cleaned_data['username'] = self.generate_random_username(length=8)
        username, email, password = cleaned_data['username'], cleaned_data['email'], cleaned_data['password']
        first_name, last_name = cleaned_data['first_name'], cleaned_data['last_name']
        role, affiliation, about_me = cleaned_data['role'], cleaned_data['affiliation'], cleaned_data['about_me']
#         if Site._meta.installed:
#             site = Site.objects.get_current()
#         else:
        site = RequestSite(request)

        new_user = OwjRegistrationProfile.objects.create_inactive_user(username, email, password, site,
                                            role=role, first_name=first_name, last_name=last_name,
                                            affiliation=affiliation, about_me=about_me)
        signals.user_registered.send(sender=self.__class__,
                                     user=new_user,
                                     request=request)
        return new_user


    def generate_random_username(self, length=30, chars=ascii_lowercase + digits, split=0, delimiter='-'):
        username = ''.join([choice(chars) for i in xrange(length)])
        if split:
            username = delimiter.join([username[start:start + split] for start in range(0, len(username), split)])
        try:
            User.objects.get(username=username)
            return self.generate_random_username(length=length, chars=chars, split=split, delimiter=delimiter)
        except User.DoesNotExist:
            return username;

    def registration_allowed(self, request):
        """
        Indicate whether account registration is currently permitted,
        based on the value of the setting ``REGISTRATION_OPEN``. This
        is determined as follows:

        * If ``REGISTRATION_OPEN`` is not specified in settings, or is
          set to ``True``, registration is permitted.

        * If ``REGISTRATION_OPEN`` is both specified and set to
          ``False``, registration is not permitted.
        
        """
        return getattr(settings, 'REGISTRATION_OPEN', True)

    def get_success_url(self, request, user):
        """
        Return the name of the URL to redirect to after successful
        user registration.
        
        """
        return ('registration_complete', (), {})


class OwjActivationView(ActivationView):

    def activate(self, request, activation_key):
        activated_user = ActivationView.activate(self, request, activation_key)
        # for reviewers keep them deactiveted as admin will activate them
        if activated_user and activated_user.role.name.lower() == "reviewer":
            activated_user.is_active = False
            activated_user.save()
        return activated_user

    def get_success_url(self, request, user):
        if user and user.role.name.lower() == "reviewer":
            return ('registration_reviewer_activation_complete', (), {})
        return ('registration_activation_complete', (), {})


def notify_admin(sender, **kwargs):
    """
    notify admins when a new user is activated
    """
    user = kwargs["user"]
    user_profile_url = os.path.join("http://", RequestSite(kwargs["request"]).domain,
                reverse("user_info", args=(user.pk,)).lstrip("/")  
            )
    context = {
            "user_profile_url" : user_profile_url 
        }
    if user.role.name.lower() == "author":
        mail = DbSendMail("UAMA")
        admin_emails = User.objects.admins().values_list("email")
        admin_emails = [email[0] for email in admin_emails]
        mail.sendmail(admin_emails, context)
    elif user.role.name.lower() == "reviewer":
        mail = DbSendMail("MAAR")
        emails = User.objects.admin_and_editors().values_list("email")
        emails = [email[0] for email in emails    ]
        mail.sendmail(emails, context)
    else:
        role = user.role and user.role.name.lower()
        logger.critical("Some user is registered in the system, where user role is {}".format(role))

    logger.info("New author is activated. Url: {}".format(context["user_profile_url"]))



def add_to_group(sender, **kwargs):
    """
    Adds the user to the group of the given role when new user is registered
    """
    user = kwargs["user"]
    if user.role.name.lower() in ["author", "reviewer"]:
        group = Group.objects.get(name__icontains=user.role.name)
        user.groups.add(group)
        logger.debug("user is added to a group")


signals.user_registered.connect(add_to_group)
signals.user_activated.connect(notify_admin)
