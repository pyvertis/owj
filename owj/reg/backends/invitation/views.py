from articles.models import InvitedUsers
from dbmail.mail import DbSendMail
from django import forms
from django.conf import settings
from django.contrib.auth import authenticate, login, get_user_model
from django.contrib.sites.models import RequestSite
from django.core import exceptions
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from main.models import Role
from reg.backends.default.views import RegistrationView
from reg.forms import OwjRegistrationForm
import logging
import os

logger = logging.getLogger("owj")

User = get_user_model()


class InvitedReviewerRegistrationForm(OwjRegistrationForm):
    """
    Invited users should not be able to edit the email and role
    """
    
    code = forms.CharField(widget=forms.HiddenInput)
    email = forms.EmailField(widget=forms.TextInput(attrs={"placeholder":"Your Email", "class":"form-control",
                            "disabled": "disabled"}),
                             label="Your Email")

    role = forms.ModelChoiceField(widget=forms.Select(attrs={"placeholder":"Register as", "class":"form-control",
                            "disabled": "disabled"}),
                             queryset=Role.objects.filter(name__in=settings.OWJ_PUBLIC_REGISTRATION_GROUPS),
                                label="Register as")


class InvitedUserRegistrationView(RegistrationView):
    """
    Registration process for invited reviewers
    """
    form_class = InvitedReviewerRegistrationForm


    def _get_invited_user(self):
        return get_object_or_404(InvitedUsers, invitation_code=self.request.REQUEST["code"])
        
    def _get_reviewer_role(self):
        try:
            role = Role.objects.get(name__istartswith="reviewer")
        except:
            logger.critical("'Reviewer' role not found in Role Table.")
            raise exceptions.ImproperlyConfigured("'Reviewer' role not found in Role Table.")
        return role

    def get_initial(self):
        """
        if url has invitation code. get the users information from invited user table 
        and prefill the form
        """
        initial = self.initial.copy()
        if self.request.GET.has_key("code"):
            invited_user = self._get_invited_user()
            role = self._get_reviewer_role()
            form_initial = {
                            "first_name": invited_user.first_name,
                            "last_name" :invited_user.last_name,
                            "email": invited_user.email,
                            "role": role.id,
                            "affiliation": invited_user.affiliation,
                            "code": self.request.GET["code"]
                            }
        
            initial.update(form_initial)
        return initial

    
    def get_form_kwargs(self, request=None, form_class=None):
        kwargs = RegistrationView.get_form_kwargs(self, request=request, form_class=form_class)
        if self.request.method in ('POST', 'PUT'):
            invited_user = self._get_invited_user()
            role = self._get_reviewer_role()
            
            data = dict(zip(self.request.POST.keys(), self.request.POST.values()))
            data["email"] = invited_user.email
            data["role"] = role.id
            kwargs.update({
                'data': data,
                'files': self.request.FILES,
            })
        return kwargs
    
    def register(self, request, **cleaned_data):
        """
        For the invited user. simple register the user and redirect him to home page.
        Send a mail to admins N editors
        """
        cleaned_data['username'] = self.generate_random_username()
        username, email, password = cleaned_data['username'], cleaned_data['email'], cleaned_data['password']
        first_name, last_name = cleaned_data['first_name'], cleaned_data['last_name']
        role, affiliation, about_me = cleaned_data['role'], cleaned_data['affiliation'], cleaned_data['about_me']

        new_user = User.objects.create_user(username, email, password, role=role, about_me=about_me,
                         first_name=first_name, last_name=last_name,
                         affiliation=affiliation
                         )

        new_user = authenticate(username=email, password=password)
        login(request, new_user)
        mail = DbSendMail("IURT")
        user_profile_url = os.path.join("http://", RequestSite(request).domain,
                reverse("admin:main_user_change", args=(new_user.pk,)).lstrip("/")  
            )
        mail_context = {"user": new_user.get_full_name(),
                        "link": user_profile_url
                    }
        editors = User.objects.editors().values_list("email", flat=True)
        admins = User.objects.admins().values_list("email", flat=True)
        emails = list(editors) + list(admins)
        
        mail.sendmail(emails, mail_context)
        
        return new_user


    def registration_allowed(self, request):
        """
        for invited users registration is always open
        """
        return True

    def get_success_url(self, request, user):
        """
        after authenticating redirect them to home page
        """
        return reverse('home')


