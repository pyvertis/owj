from owj.utils import unique_slugify
from django.core.urlresolvers import reverse
import random, string

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model
from filer.models import File
from taggit.managers import TaggableManager
from sortedm2m.fields import SortedManyToManyField
import datetime


User = get_user_model()

def random_string(count):
    """
    Returns a random string containing "count" number of characters 
    """
    return "".join(random.sample("{}{}".format(string.letters, string.digits), count))


def get_pdf_upload_path(instance, filename):
    """
    Returns the filepath for new pdf file uploads
    """
    return "{}/article_pdf/{}/{}".format(instance.__class__.__name__, random_string(8), filename)

# Create your models here.

class ExtAuthor(models.Model):
    """
    Article authors added by main article author.
    """
    first_name = models.CharField('first name', max_length=30, blank=True)
    last_name = models.CharField('last name', max_length=30, blank=True)
    email = models.EmailField('email', blank=True)
    affiliation = models.CharField('affiliation', max_length=250, blank=True)
    is_author = models.BooleanField('is_author', default=False, blank=False)

    def __unicode__(self):
        return self.full_name()

    def full_name(self):
        return "{} {}".format(self.first_name, self.last_name)


class ExtReviewer(models.Model):
    """
    Suggested reviewers for article.
    """
    first_name = models.CharField('first name', max_length=30, blank=True)
    last_name = models.CharField('last name', max_length=30, blank=True)
    email = models.EmailField('email', blank=True)
    affiliation = models.CharField('affiliation', max_length=250, blank=True)

    def __unicode__(self):
        return self.first_name


class Article(models.Model):
    """
    Article submitted by author
    """

    ARTICLE_STATUS = (('open', 'Open'), ('inreview', 'In Review'), ('accepted', 'Accepted'),
                      ('rejected', 'Rejected'), ('reopened', 'Reopened'), ('paid', 'Paid'),
                      ('typeset', 'Typeset'), ('published', 'Published'))

    author = models.ForeignKey(User, help_text="Main author of article")

    title = models.CharField(max_length=256)
    abstract = models.TextField(blank=True, max_length=4096)
    pdf = models.ForeignKey(File , help_text="article's pdf file")
    # we can use external file manager tools like django filer
    ext_authors = SortedManyToManyField(ExtAuthor, help_text="article authors")
    ext_reviewers = models.ManyToManyField(ExtReviewer, help_text="Reviewers suggested by author")
    status = models.CharField(choices=ARTICLE_STATUS, max_length=32, default=ARTICLE_STATUS[0][0])
    paid = models.BooleanField(default=False, help_text="Whether author has paid for this article to publish.")
    in_bidding_pool = models.BooleanField(default=False, help_text="Whetear article is in bidding pool or not")
    status_date = models.DateField(auto_now_add=True, help_text='date when status was changed last time')
    publish_date = models.DateField(null=True)
    # meta fields
    modified = models.DateTimeField(auto_now=True, auto_now_add=True)
    created = models.DateTimeField(auto_now_add=True)
    slug = models.SlugField(max_length=256, unique=True)

    file_download_count = models.PositiveIntegerField(default=0,
                    help_text="Number of times article pdf have been downloaded after it got published.")
    author_can_view_reviews = models.BooleanField(default=False,
                    help_text="This is set when Editor writes a review for first time. Author can view reviews \
                    \ only when editor writes a review")

    key_words = TaggableManager()

    class Meta:
        permissions = (
                       ("can_accept_article", "can accept article"),
                       ("can_move_article_bidding_pool", "can move article to bidding pool")
                    )

    def __unicode__(self):
        return self.title

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        unique_slugify(self, self.title, "slug")
        return models.Model.save(self, force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

    def update_file(self, newFile):
        oldFile = File.objects.get(id=self.pdf.id)
        self.pdf = newFile
        self.save()
        oldFile.delete()

    def get_absolute_url(self):
        """
        Returns url for article detail page
        """
        return reverse("article_detail_page", args=(self.slug,))

    def pdf_url(self):
        """
        returns the url for pdf download
        """
        return reverse("article_pdf_download", args=(self.id,))

    def delete_ext_authors(self):
        """
        deletes ext authors.
        """
        authors = self.ext_authors.all()
        self.ext_authors.clear()
        for author in authors:
            author.delete()
        self.save()

    def delete_ext_reviewers(self):
        """
        deletes ext reviewers.
        """
        reviewers = self.ext_reviewers.all()
        self.ext_reviewers.clear()
        for reviewer in reviewers:
            reviewer.delete()
        self.save()

    def can_author_pay(self):
        """
        Returns whether author can or can not make payment for this article.
        """
        return not self.paid and (self.status == 'accepted' or self.status == 'typeset')

    def is_published(self):
        """
        Returns True/False based on status and publish_date fields
        Article is said to be published, if status=='published' and 
        publish_date is before or equal to current date.
        """
        if self.status == "published" and self.publish_date < datetime.datetime.now() :
            return True
        else:
            return False

class ArticleReviewRequest(models.Model):
    """
    Article review request.
    """

    REQUEST_STATUS = (('requested', 'Requested'), ('accepted', 'Accepted'))

    reviewer = models.ForeignKey(User , help_text="Main author of article")
    article = models.ForeignKey(Article)
    status = models.CharField(choices=REQUEST_STATUS, max_length=32, default=REQUEST_STATUS[0][0])
    # meta fields
    modified = models.DateTimeField(auto_now=True, auto_now_add=True)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.reviewer.first_name + ' - ' + self.article.title


class ArticleReviewers(models.Model):
    """
        Reviwers assigned to article by editor
    """
    reviewer = models.ForeignKey(User)
    article = models.ForeignKey(Article)

    # meta fields
    modified = models.DateTimeField(auto_now=True, auto_now_add=True)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.article.title + " - " + self.reviewer.first_name


class ArticleReview(models.Model):
    """
        Article review
    """
    ARTICLE_REVIEW_STATUS = (('accepted', 'Accepted'), ('rejected', 'Rejected'), ('revise', 'Revise'))

# needs to be reviewed
    article = models.ForeignKey(Article)
    comment = models.TextField('review', blank=True, help_text='Review text')
    recommendation = models.CharField(choices=ARTICLE_REVIEW_STATUS, max_length=32, blank=True, help_text='Review given by reviewer')
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ("-created",)
        permissions = (("can_view_all_reviews", "Can view all reviews"),)

class ReviewerReview(ArticleReview):
    """
        Reviwes given by reviewers and editor as a reviewer
    """
    reviewer = models.ForeignKey(User)

    def __unicode__(self):
        return self.comment

class EditorReview(ArticleReview):
    """
        Reviwes given by editor. Reviews like final decision about article.
    """
    reviewer = models.ForeignKey(User)

    class Meta:
        ordering = ("created",)


class Volume(models.Model):
    """
    Volume - Maintains all the Volumes for Journal
    """
    name = models.CharField(_('name'), max_length=10, blank=False)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.name


class Issue(models.Model):
    """
    Issue - contains list of articles.
    """
    volume = models.ForeignKey(Volume)
    title = models.CharField(_('issue title'), max_length=128)  # recheck in doc
    issue_number = models.IntegerField(_('issue number'))
    articles = models.ManyToManyField(Article)  # this needs to maintain the order of articles
    # so it should be replaced with some other field
    is_published = models.BooleanField(default=False)
    publish_date = models.DateField(null=True)
    editor = models.ForeignKey(User)
    created = models.DateTimeField(auto_now_add=True)
    slug = models.SlugField(max_length=256, unique=True)

    def __unicode__(self):
        return self.title

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        unique_slugify(self, self.title, "slug")
        return models.Model.save(self, force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)



class Transaction(models.Model):
    """
    Transaction table for amount paid by author against article to publish it.
    """
    author = models.ForeignKey(User)
    article = models.ForeignKey(Article)
    paypal_transaction_id = models.CharField(_('paypal transaction id'), max_length=32)
    paypal_payer_id = models.CharField(_('paypal payer id'), max_length=32, blank=True, null=True)
    amount = models.IntegerField(_('amount in cents'))
    created = models.DateTimeField(auto_now_add=True)


class InvitedUsers(models.Model):
    """
    Not system users, who are invited to join OWJ
    """
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.EmailField()
    affiliation = models.CharField(max_length=256, blank=True, null=True)
    invitation_code = models.CharField(max_length=10, unique=True)
    message = models.CharField(max_length=500)

    created_date = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.get_display_name()

    def get_display_name(self):
        return "{} {}".format(self.first_name, self.last_name)

