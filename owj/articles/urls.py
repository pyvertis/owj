from django.conf.urls import patterns, url
from articles.views import SubmitPaper, ArticleView, ArticleReviewRequestView, \
    ArticleMoveBiddingPoolView, ArticleTypeSetView, artice_pdf_download_view, \
    PaperPayment, PaymentCallback

urlpatterns = patterns('',
    url(r'^author/submit-paper/$', SubmitPaper.as_view(template_name='author/submit_paper.html'),
        name='submit_paper'),
    url(r'^author/article/initpayment/$', PaperPayment.as_view(),
        name='paper_payment'),
    url(r'^article/(?P<slug>[\w-]+)/$', ArticleView.as_view(), name="article_detail_page"),
    url(r'^article/(?P<article_id>\d+)/download$', artice_pdf_download_view, name="article_pdf_download"),
    url(r'^reviewer/article/request/$', ArticleReviewRequestView.as_view()),
    url(r'^editor/article/movebiddingpool/$', ArticleMoveBiddingPoolView.as_view()),
    url(r'^editor/article/typeset/$', ArticleTypeSetView.as_view()),
    url(r'^paymentcallback/(?P<article_id>\d+)/$', PaymentCallback.as_view(),
        name='payment_callback'),
)


