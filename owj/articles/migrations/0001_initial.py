# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ExtAuthor'
        db.create_table(u'articles_extauthor', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, blank=True)),
            ('affiliation', self.gf('django.db.models.fields.CharField')(max_length=250, blank=True)),
            ('is_author', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'articles', ['ExtAuthor'])

        # Adding model 'ExtReviewer'
        db.create_table(u'articles_extreviewer', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, blank=True)),
            ('affiliation', self.gf('django.db.models.fields.CharField')(max_length=250, blank=True)),
        ))
        db.send_create_signal(u'articles', ['ExtReviewer'])

        # Adding model 'Article'
        db.create_table(u'articles_article', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('author', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.User'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('abstract', self.gf('django.db.models.fields.TextField')(max_length=4096, blank=True)),
            ('pdf', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['filer.File'])),
            ('status', self.gf('django.db.models.fields.CharField')(default='open', max_length=32)),
            ('paid', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('in_bidding_pool', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('status_date', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
            ('publish_date', self.gf('django.db.models.fields.DateField')(null=True)),
            ('modified', self.gf('django.db.models.fields.DateField')(auto_now=True, auto_now_add=True, blank=True)),
            ('created', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'articles', ['Article'])


        # Adding SortedM2M table for field ext_authors on 'Article'
        db.create_table(u'articles_article_ext_authors', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('article', models.ForeignKey(orm[u'articles.article'], null=False)),
            ('extauthor', models.ForeignKey(orm[u'articles.extauthor'], null=False)),
            ('sort_value', models.IntegerField())
        ))
        db.create_unique(u'articles_article_ext_authors', ['article_id', 'extauthor_id'])
        # Adding M2M table for field ext_reviewers on 'Article'
        m2m_table_name = db.shorten_name(u'articles_article_ext_reviewers')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('article', models.ForeignKey(orm[u'articles.article'], null=False)),
            ('extreviewer', models.ForeignKey(orm[u'articles.extreviewer'], null=False))
        ))
        db.create_unique(m2m_table_name, ['article_id', 'extreviewer_id'])

        # Adding model 'ArticleReviewRequest'
        db.create_table(u'articles_articlereviewrequest', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('reviewer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.User'])),
            ('article', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['articles.Article'])),
            ('status', self.gf('django.db.models.fields.CharField')(default='requested', max_length=32)),
            ('modified', self.gf('django.db.models.fields.DateField')(auto_now=True, auto_now_add=True, blank=True)),
            ('created', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'articles', ['ArticleReviewRequest'])

        # Adding model 'ArticleReviewers'
        db.create_table(u'articles_articlereviewers', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('reviewer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.User'])),
            ('article', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['articles.Article'])),
            ('modified', self.gf('django.db.models.fields.DateField')(auto_now=True, auto_now_add=True, blank=True)),
            ('created', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'articles', ['ArticleReviewers'])

        # Adding model 'ArticleReview'
        db.create_table(u'articles_articlereview', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('article', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['articles.Article'])),
            ('comment', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('recommendation', self.gf('django.db.models.fields.CharField')(max_length=32, blank=True)),
            ('created', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'articles', ['ArticleReview'])

        # Adding model 'ReviewerReview'
        db.create_table(u'articles_reviewerreview', (
            (u'articlereview_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['articles.ArticleReview'], unique=True, primary_key=True)),
            ('reviewer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.User'])),
        ))
        db.send_create_signal(u'articles', ['ReviewerReview'])

        # Adding model 'EditorReview'
        db.create_table(u'articles_editorreview', (
            (u'articlereview_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['articles.ArticleReview'], unique=True, primary_key=True)),
            ('reviewer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.User'])),
        ))
        db.send_create_signal(u'articles', ['EditorReview'])

        # Adding model 'Volume'
        db.create_table(u'articles_volume', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('created', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'articles', ['Volume'])

        # Adding model 'Issue'
        db.create_table(u'articles_issue', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('volume', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['articles.Volume'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('issue_number', self.gf('django.db.models.fields.IntegerField')()),
            ('is_published', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('publish_date', self.gf('django.db.models.fields.DateField')(null=True)),
            ('editor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.User'])),
            ('created', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'articles', ['Issue'])

        # Adding M2M table for field articles on 'Issue'
        m2m_table_name = db.shorten_name(u'articles_issue_articles')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('issue', models.ForeignKey(orm[u'articles.issue'], null=False)),
            ('article', models.ForeignKey(orm[u'articles.article'], null=False))
        ))
        db.create_unique(m2m_table_name, ['issue_id', 'article_id'])

        # Adding model 'Transaction'
        db.create_table(u'articles_transaction', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('author', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.User'])),
            ('article', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['articles.Article'])),
            ('paypal_transaction_id', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('amount', self.gf('django.db.models.fields.IntegerField')()),
            ('created', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'articles', ['Transaction'])


    def backwards(self, orm):
        # Deleting model 'ExtAuthor'
        db.delete_table(u'articles_extauthor')

        # Deleting model 'ExtReviewer'
        db.delete_table(u'articles_extreviewer')

        # Deleting model 'Article'
        db.delete_table(u'articles_article')

        # Removing M2M table for field ext_authors on 'Article'
        db.delete_table(db.shorten_name(u'articles_article_ext_authors'))

        # Removing M2M table for field ext_reviewers on 'Article'
        db.delete_table(db.shorten_name(u'articles_article_ext_reviewers'))

        # Deleting model 'ArticleReviewRequest'
        db.delete_table(u'articles_articlereviewrequest')

        # Deleting model 'ArticleReviewers'
        db.delete_table(u'articles_articlereviewers')

        # Deleting model 'ArticleReview'
        db.delete_table(u'articles_articlereview')

        # Deleting model 'ReviewerReview'
        db.delete_table(u'articles_reviewerreview')

        # Deleting model 'EditorReview'
        db.delete_table(u'articles_editorreview')

        # Deleting model 'Volume'
        db.delete_table(u'articles_volume')

        # Deleting model 'Issue'
        db.delete_table(u'articles_issue')

        # Removing M2M table for field articles on 'Issue'
        db.delete_table(db.shorten_name(u'articles_issue_articles'))

        # Deleting model 'Transaction'
        db.delete_table(u'articles_transaction')


    models = {
        u'articles.article': {
            'Meta': {'object_name': 'Article'},
            'abstract': ('django.db.models.fields.TextField', [], {'max_length': '4096', 'blank': 'True'}),
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.User']"}),
            'created': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'ext_authors': ('sortedm2m.fields.SortedManyToManyField', [], {'to': u"orm['articles.ExtAuthor']", 'symmetrical': 'False'}),
            'ext_reviewers': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['articles.ExtReviewer']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_bidding_pool': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'modified': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'paid': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'pdf': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['filer.File']"}),
            'publish_date': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'open'", 'max_length': '32'}),
            'status_date': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        u'articles.articlereview': {
            'Meta': {'object_name': 'ArticleReview'},
            'article': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['articles.Article']"}),
            'comment': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'created': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'recommendation': ('django.db.models.fields.CharField', [], {'max_length': '32', 'blank': 'True'})
        },
        u'articles.articlereviewers': {
            'Meta': {'object_name': 'ArticleReviewers'},
            'article': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['articles.Article']"}),
            'created': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'reviewer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.User']"})
        },
        u'articles.articlereviewrequest': {
            'Meta': {'object_name': 'ArticleReviewRequest'},
            'article': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['articles.Article']"}),
            'created': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'reviewer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.User']"}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'requested'", 'max_length': '32'})
        },
        u'articles.editorreview': {
            'Meta': {'object_name': 'EditorReview', '_ormbases': [u'articles.ArticleReview']},
            u'articlereview_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['articles.ArticleReview']", 'unique': 'True', 'primary_key': 'True'}),
            'reviewer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.User']"})
        },
        u'articles.extauthor': {
            'Meta': {'object_name': 'ExtAuthor'},
            'affiliation': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_author': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'})
        },
        u'articles.extreviewer': {
            'Meta': {'object_name': 'ExtReviewer'},
            'affiliation': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'})
        },
        u'articles.issue': {
            'Meta': {'object_name': 'Issue'},
            'articles': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['articles.Article']", 'symmetrical': 'False'}),
            'created': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'editor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.User']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_published': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'issue_number': ('django.db.models.fields.IntegerField', [], {}),
            'publish_date': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'volume': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['articles.Volume']"})
        },
        u'articles.reviewerreview': {
            'Meta': {'object_name': 'ReviewerReview', '_ormbases': [u'articles.ArticleReview']},
            u'articlereview_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['articles.ArticleReview']", 'unique': 'True', 'primary_key': 'True'}),
            'reviewer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.User']"})
        },
        u'articles.transaction': {
            'Meta': {'object_name': 'Transaction'},
            'amount': ('django.db.models.fields.IntegerField', [], {}),
            'article': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['articles.Article']"}),
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.User']"}),
            'created': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'paypal_transaction_id': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'articles.volume': {
            'Meta': {'object_name': 'Volume'},
            'created': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'filer.file': {
            'Meta': {'object_name': 'File'},
            '_file_size': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'folder': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'all_files'", 'null': 'True', 'to': u"orm['filer.Folder']"}),
            'has_all_mandatory_data': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_public': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'modified_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '255', 'blank': 'True'}),
            'original_filename': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'owned_files'", 'null': 'True', 'to': u"orm['main.User']"}),
            'polymorphic_ctype': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'polymorphic_filer.file_set'", 'null': 'True', 'to': u"orm['contenttypes.ContentType']"}),
            'sha1': ('django.db.models.fields.CharField', [], {'default': "u''", 'max_length': '40', 'blank': 'True'}),
            'uploaded_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'filer.folder': {
            'Meta': {'ordering': "(u'name',)", 'unique_together': "((u'parent', u'name'),)", 'object_name': 'Folder'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            u'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'modified_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'owner': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'filer_owned_folders'", 'null': 'True', 'to': u"orm['main.User']"}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'children'", 'null': 'True', 'to': u"orm['filer.Folder']"}),
            u'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'uploaded_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'main.role': {
            'Meta': {'object_name': 'Role'},
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        u'main.user': {
            'Meta': {'object_name': 'User'},
            'about_me': ('django.db.models.fields.TextField', [], {'max_length': '2048', 'blank': 'True'}),
            'affiliation': ('django.db.models.fields.TextField', [], {'max_length': '2048', 'blank': 'True'}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'role': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Role']"}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        }
    }

    complete_apps = ['articles']