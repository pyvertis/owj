from django.shortcuts import redirect, get_object_or_404
from django.views.generic.base import View, TemplateView
from django.core.urlresolvers import reverse, reverse_lazy
from django.views.decorators.csrf import ensure_csrf_cookie
from django.utils.decorators import method_decorator
from filer.models import File
from django.core.files.uploadedfile import SimpleUploadedFile
from filer.utils.files import UploadException
from articles.models import Article, ExtAuthor, ExtReviewer, \
    ArticleReviewRequest, Transaction
from dbmail.mail import DbSendMail
import json
from django.http.response import HttpResponse, HttpResponseRedirect
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import permission_required, login_required
from django.views.generic.detail import DetailView
import mimetypes
import datetime
import os
import urllib
import urlparse
from datetime import datetime
from owj.settings.base import PUBLISH_ARTICLE_COST, \
    PAYPAL_DEFAULTS, PAYPAL_MODE, PAYPAL_PAYMENT_REQUEST
import urllib2
import httplib
from django.contrib.sites.models import RequestSite

def urlencode(data):
    """Utilities for the paypal_express_checkout."""
    for key, value in data.iteritems():
        data[key] = unicode(value).encode('utf-8')
    return urllib.urlencode(data)


User = get_user_model()

# Create your views here.

def _create_article_file(request):
    '''
    Creates new file of File type from request and return same object.
    '''
    pdfFile = request.FILES['file']

    if request.is_ajax():
    # the file is stored raw in the request
        upload = SimpleUploadedFile(name=pdfFile.name, content=pdfFile.file)
    else:
        if len(request.FILES) == 1:
            upload = list(request.FILES.values())[0]
        else:
            raise UploadException("AJAX request not valid: Bad Upload")

    fileObj = File.objects.create(owner=request.user, original_filename=pdfFile.name, file=upload)
    fileObj.save()
    return fileObj


def paypal_checkout(article, *args, **kwargs):
    postdata = PAYPAL_DEFAULTS.copy()
    request = kwargs["request"]
    host = os.path.join("http://", RequestSite(request).domain)
    postdata.update({'AMT':PUBLISH_ARTICLE_COST,
                     'PAYMENTREQUEST_0_AMT':PUBLISH_ARTICLE_COST,
                     'PAYMENTREQUEST_0_ITEMAMT':PUBLISH_ARTICLE_COST,
                     'L_PAYMENTREQUEST_0_AMT0': PUBLISH_ARTICLE_COST,
                     'L_PAYMENTREQUEST_0_NAME0': article.title,
                     'L_PAYMENTREQUEST_0_QTY0': 1,
                     'L_PAYMENTREQUEST_0_NUMBER0':article.id,
                     'RETURNURL':host + reverse('payment_callback', args=(article.id,)),
                     'CANCELURL':host + reverse('author_submissions'),
                     'METHOD':'SetExpressCheckout',
                     'PAYMENTREQUEST_0_PAYMENTACTION': PAYPAL_PAYMENT_REQUEST,
                    })
    responce = call_pyapal(postdata)
    if responce:
        ack = responce.get('ACK', '')[0]
        if ack.upper() == 'SUCCESS':
            token = responce.get('TOKEN', None)[0]
            if PAYPAL_MODE == 'sandbox':
                redirecturl = 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=' + token
            else:
                redirecturl = 'https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=' + token

            return HttpResponse('{"status" : "ok","redirect":"' + redirecturl + '"}', content_type='application/json')
        else:
            return HttpResponse('{"status" : "error"}', content_type='application/json')
    else:
        return HttpResponse('{"status" : "error"}', content_type='application/json')


def call_pyapal(postdata):

    if PAYPAL_MODE == 'sandbox':
        api_url = "https://api-3t.sandbox.paypal.com/nvp"
    else:
        api_url = "https://api-3t.paypal.com/nvp"

    try:
        response = urllib2.urlopen(api_url, data=urlencode(postdata))
    except (
            urllib2.HTTPError,
            urllib2.URLError,
            httplib.HTTPException), ex:
       return False
    else:
        parsed_response = urlparse.parse_qs(response.read())
        return parsed_response


class SubmitPaper(TemplateView):
    '''
    View for article submission
    '''
    http_method_names = ['post']

    @method_decorator(ensure_csrf_cookie)
    @method_decorator(login_required(login_url=reverse_lazy("owj_login")))
    def dispatch(self, request, *args, **kwargs):
        return TemplateView.dispatch(self, request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        try:
            fileObj = None
            atricle_id = None
            article = json.loads(request.POST['article'])
            atricle_id = article.get('id')

            if atricle_id:
                # edit mode
                articleObj = Article.objects.filter(author=request.user, id=atricle_id)
                if len(articleObj) < 1:
                    # article does not belong to current logged in user.
                    raise Exception("Article not found.")

                articleObj = articleObj[0]
                if len(request.FILES) == 1:
                    # only delete if new file is provided.
                    fileObj = _create_article_file(request)
                    articleObj.update_file(fileObj)

                articleObj.delete_ext_authors()
                articleObj.delete_ext_reviewers()
                articleObj.title = article.get("title")
                articleObj.abstract = article.get("abstract")
            else:
                # create mode
                fileObj = _create_article_file(request)

                articleObj = Article(author=request.user, pdf=fileObj,
                                title=article.get("title"),
                                abstract=article.get("abstract"))

            articleObj.save()

            for author in article.get("authors", []):
                authorObj = ExtAuthor.objects.create(
                          first_name=author.get('first_name'),
                          last_name=author.get('last_name'),
                          email=author.get('email'),
                          affiliation=author.get('affiliation'),
                          is_author=bool(author.get('is_author', False))
                        )

                articleObj.ext_authors.add(authorObj)

            for reviewer in article.get("reviewers", []):
                reviewerObj = ExtReviewer.objects.create(
                        first_name=reviewer.get('first_name'),
                        last_name=reviewer.get('last_name'),
                        email=reviewer.get('email'),
                        affiliation=reviewer.get('affiliation')
                        )

                articleObj.ext_reviewers.add(reviewerObj)

            articleObj.key_words.clear()
            for word in article.get("keywords", []):
                articleObj.key_words.add(word)

            articleObj.save()

            self.notify_article_submit(request.user, articleObj)

            return HttpResponse('{"status" : "ok"}', content_type='application/json')

        except Exception as e:
            # check if we are not in edit mode and article has id then only delete.
            if atricle_id is None and articleObj and articleObj.id:
                articleObj.delete_ext_authors()
                articleObj.delete_ext_reviewers()
                articleObj.delete()

            if atricle_id is None and fileObj and fileObj.id:
                fileObj.delete()
            return HttpResponse('{"status" : "error"}', content_type='application/json')


    def notify_article_submit(self, user, article):
        request_mail = DbSendMail("ASNA")
        recipients = [ editor.email for editor in User.objects.editors()]

        context = {"full_name":user.get_full_name(),
                   "email":user.email,
                   "article_title":article.title,
                  }
        request_mail.sendmail(recipients, context)


class ArticleView(DetailView):
    model = Article

    template_name = "article/article_page.html"


def artice_pdf_download_view(request, article_id):
    """
    To download the article pdf file.
    """
    article = get_object_or_404(Article, id=article_id)

    # update the article download count.
    if article.status == "published" and article.publish_date <= datetime.datetime.now():
        article.file_download_count = article.file_download_count + 1
        article.save()

    original_filename = os.path.basename(article.pdf.file.name)
    response = HttpResponse(article.pdf.file.read())
    type, encoding = mimetypes.guess_type(original_filename)
    if type is None:
        type = 'application/octet-stream'
    response['Content-Type'] = type
    response['Content-Length'] = str(article.pdf.file.size)
    if encoding is not None:
        response['Content-Encoding'] = encoding

    # To inspect details for the below code, see http://greenbytes.de/tech/tc2231/
    if u'WebKit' in request.META['HTTP_USER_AGENT']:
        # Safari 3.0 and Chrome 2.0 accepts UTF-8 encoded string directly.
        filename_header = 'filename=%s' % original_filename.encode('utf-8')
    elif u'MSIE' in request.META['HTTP_USER_AGENT']:
        # IE does not support internationalized filename at all.
        # It can only recognize internationalized URL, so we do the trick via routing rules.
        filename_header = ''
    else:
        # For others like Firefox, we follow RFC2231 (encoding extension in HTTP headers).
        filename_header = 'filename*=UTF-8\'\'%s' % urllib.quote(original_filename.encode('utf-8'))
    response['Content-Disposition'] = 'attachment; ' + filename_header
    return response


class ArticleReviewRequestView(View):
    '''
    Handle reviewer article review request.
    '''
    http_method_names = ['post']

    @method_decorator(ensure_csrf_cookie)
    @method_decorator(login_required(login_url=reverse_lazy("owj_login")))
    @method_decorator(permission_required(perm='articles.add_articlereviewrequest', raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return View.dispatch(self, request, *args, **kwargs)

    def post(self, request, *args, **kwargs):

        try :
            article_id = request.POST['article_id']

            article = Article.objects.get(id=article_id)
            if not article.in_bidding_pool:
                raise Exception("Article not in bidding pool.")

            reviewRequest = ArticleReviewRequest.objects.filter(reviewer=request.user, article_id=article_id)

            if reviewRequest:
                # request already exists.
                return HttpResponse('{"status" : "exist"}', content_type='application/json')


            newReviewRequest = ArticleReviewRequest.objects.create(reviewer=request.user, article=article)
            newReviewRequest.save()
            self.notify_article_review_request(request.user, article)

            return HttpResponse('{"status" : "requested"}', content_type='application/json')

        except Exception as e:

            return HttpResponse('{"status" : "error"}', content_type='application/json')


    def notify_article_review_request(self, user, article):
        request_mail = DbSendMail("RRAR")
        recipients = [ user.email for user in User.objects.editors()]

        context = {"reviewer_name":user.get_full_name(),
                   "reviewer_email":user.email,
                   "article_title":article.title,
                  }

        request_mail.sendmail(recipients, context)


class ArticleMoveBiddingPoolView(View):
    '''
    Move article to bidding pool
    '''
    http_method_names = ['post']

    @method_decorator(ensure_csrf_cookie)
    @method_decorator(login_required(login_url=reverse_lazy("owj_login")))
    @method_decorator(permission_required(perm='articles.can_move_article_bidding_pool', raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return View.dispatch(self, request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return redirect(reverse("owj_login"))

        try :
            article_id = request.POST['article_id']
            article = Article.objects.get(id=article_id)

            if article.in_bidding_pool:
                # article already in bidding pool.
                return HttpResponse('{"status" : "exist"}', content_type='application/json')

            article.in_bidding_pool = True
            article.save()

            return HttpResponse('{"status" : "ok"}', content_type='application/json')

        except Exception as e:

            return HttpResponse('{"status" : "error"}', content_type='application/json')


class ArticleTypeSetView(View):
    '''
    View to handle article update/typeset post request.
    '''
    http_method_names = ['post']

    @method_decorator(ensure_csrf_cookie)
    @method_decorator(login_required(login_url=reverse_lazy("owj_login")))
    @method_decorator(permission_required(perm='articles.change_article', raise_exception=True))
    def dispatch(self, request, *args, **kwargs):
        return View.dispatch(self, request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        try :
            article_id = request.POST['article_id']
            article = Article.objects.get(id=article_id)

            if not article:
                raise Exception('Article not found.')

            status = request.POST['status']

            valid_status = [i[0] for i in article.ARTICLE_STATUS]
            if status not in valid_status:
                raise Exception('Invalid status.')
            article.status = status

            article.save()
            if len(request.FILES) == 1:
                fileObj = _create_article_file(request)
                article.update_file(fileObj)

            return HttpResponse('{"status" : "ok"}', content_type='application/json')

        except Exception as e:

            return HttpResponse('{"status" : "error"}', content_type='application/json')

class PaperPayment(View):
    '''
    View to initialize article payment.
    '''
    http_method_names = ['post']

    @method_decorator(login_required(login_url=reverse_lazy("owj_login")))
    def dispatch(self, request, *args, **kwargs):
        return View.dispatch(self, request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        try :
            article_id = request.POST['article_id']
            article = Article.objects.filter(author=request.user, id=article_id)

            if len(article) < 1:
                    # article does not belong to current logged in user.
                    raise Exception("Article not found.")

            article = article[0]
            return paypal_checkout(article, request=request)

        except Exception as e:

            return HttpResponse('{"status" : "error"}', content_type='application/json')


class PaymentCallback(TemplateView):
    '''
    View to handle paypal callback.
    '''

    template_name = 'article/payment_callback.html'

    def get(self, request, *args, **kwargs):
        article_id = kwargs.get('article_id', None)

        if request.GET['token'] and request.GET['PayerID']:
            PayerID = request.GET['PayerID']

            postdata = PAYPAL_DEFAULTS.copy()

            postdata.update({'TOKEN':request.GET['token'],
                             'PAYERID':PayerID,
                             'AMT':PUBLISH_ARTICLE_COST,
                             'METHOD':'DoExpressCheckoutPayment',
                             'PAYMENTACTION': PAYPAL_PAYMENT_REQUEST,
                            })
            responce = call_pyapal(postdata)
            if responce:
                ack = responce.get('ACK', '')[0]
                if (ack.upper() == 'SUCCESS' or ack.upper() == 'SUCCESSWITHWARNING'):
                    status = responce.get('PAYMENTSTATUS', '')[0]
                    if (status.upper() == 'COMPLETED' or status.upper() == 'PENDING'):
                        amount = int(float(responce.get('AMT', '')[0]) * 100)

                        t_id = responce.get('TRANSACTIONID', '')[0]
                        t_count = Transaction.objects.filter(paypal_transaction_id=t_id).count()

                        if t_count:
                            context = TemplateView.get_context_data(self, **kwargs)
                            context["status"] = "warning"
                            context["msg"] = responce.get('L_LONGMESSAGE0', '')[0]
                            return self.render_to_response(context)

                        article = Article.objects.get(id=int(article_id))

                        transaction = Transaction(author=request.user,
                                     article=article,
                                     paypal_transaction_id=t_id,
                                     paypal_payer_id=str(PayerID),
                                     amount=amount)
                        transaction.save()

                        article = Article.objects.get(id=int(article_id))
                        article.status = 'paid'
                        article.paid = True
                        article.status_date = datetime.now()
                        article.save()

                        context = TemplateView.get_context_data(self, **kwargs)
                        context["status"] = "ok"
                        context["msg"] = "Transaction completed successfully."
                        return self.render_to_response(context)

                    else:
                        context = TemplateView.get_context_data(self, **kwargs)
                        context["status"] = "error"
                        context["msg"] = responce.get('L_LONGMESSAGE0', '')[0] or """An error occurred while 
                                                completing transaction."""
                        return self.render_to_response(context)


                else:
                    context = TemplateView.get_context_data(self, **kwargs)
                    context["status"] = "error"
                    context["msg"] = responce.get('L_LONGMESSAGE0', '')[0] or """An error occurred while 
                                                completing transaction."""
                    return self.render_to_response(context)
            else:
                context = TemplateView.get_context_data(self, **kwargs)
                context["status"] = "error"
                context["msg"] = responce.get('L_LONGMESSAGE0', '')[0] or """An error occurred 
                                            while completing transaction."""
                return self.render_to_response(context)

        else:
            context = TemplateView.get_context_data(self, **kwargs)
            context["status"] = "error"
            context["msg"] = "An error occurred while completing transaction."
            return self.render_to_response(context)

