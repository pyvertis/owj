from django.dispatch import Signal
from dbmail.mail import DbSendMail
from django.contrib.auth import get_user_model
from datetime import datetime
User = get_user_model()


review_written_signal = Signal(providing_args=["review_obj"])
publish_issue_articles_signal = Signal(providing_args=["issue"])


def send_mail(sender, **kwargs):
    """
    Send mail to editors when a new review is written
    """
    mail = DbSendMail("MERW")
    review_obj = kwargs["review_obj"]
    context = {"article_title" :review_obj.article.title,
               "comment": review_obj.comment,
               "recommendation": review_obj.recommendation}
    emails = User.objects.editors().values_list("email")
    recipient_list = [email[0] for email in emails]
    mail.sendmail(recipient_list, context)

def publish_issue_articles(sender, **kwargs):
    """
    Publish issue articles when issue is published.
    """
    issue = kwargs["issue"]
    issue_articles = issue.articles.all()

    for article in issue_articles:
        article.status = 'published'
        article.status_date = datetime.now()
        article.publish_date = issue.publish_date
        article.save()


review_written_signal.connect(send_mail)
publish_issue_articles_signal.connect(publish_issue_articles)

