from django.contrib import admin
from .models import Article, Issue, Volume, ArticleReviewers, ReviewerReview, \
         EditorReview, ExtAuthor, ArticleReviewRequest
from main.models import Role
from owj.admin import admin_site
from articles.models import InvitedUsers

# Register your models here.

class ArticleAdmin(admin.ModelAdmin):
    list_display = ("title", "status", "status_date", "paid")


#registration to admin site
admin_site.register(Volume)
admin_site.register(InvitedUsers)

#registration to developer site
admin.site.register(InvitedUsers)
admin.site.register(Article, ArticleAdmin)
admin.site.register(Issue, admin.ModelAdmin)
admin.site.register(Volume, admin.ModelAdmin)
admin.site.register(ArticleReviewers, admin.ModelAdmin)
admin.site.register(ReviewerReview, admin.ModelAdmin)
admin.site.register(EditorReview, admin.ModelAdmin)
admin.site.register(ExtAuthor, admin.ModelAdmin)
admin.site.register(ArticleReviewRequest, admin.ModelAdmin)
admin.site.register(Role, admin.ModelAdmin)
