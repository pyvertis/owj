'use strict';
//right all the common services here
var services = angular.module("owjApp.services", []);

/*Provides information of current logged in user*/
services.factory("currentUser", ["Restangular", "$log", "$q", 
  function(Restangular, $log, $q){
	
	var _user = $q.defer();

	var user = null;
	Restangular.one("me").get().then(function(response){
		_user.resolve(response);
		 user = response;
	});

	/* add additional methods to current user */
	Restangular.extendModel("me", function(model){
		/*method to check user permissions*/
		model.has_perm = function(name){
			if(model.permissions.indexOf(name) != -1){
				return true;
			}else{
				return false;
			} 

		};
	
		return model;
	});
		
	return {"getUser":function(){return user},
	"promise": _user.promise
	};
	
}]);


/*service for accessing django app urls*/
owjApp.service("DJUrls", function(){
	var urls = Urls;
	//returns true, if given url is set in brower;
	urls.is_active_url=function(url){
		return location.pathname == url;
	};
	
	return urls;
});



/*Provides information of current logged in user*/
services.service("reviewService", ["Restangular", "$log", "$q", "$cookies","currentUser",
  function(Restangular, $log, $q, $cookies, currentUser){
	
	var _service_url = {
			editor_create : "editors/reviews/",
			editor_list: "reviews/",
			reviewer: "reviewers/reviews/",
			author_list:"reviews/"
	};
	var user = currentUser.getUser();
	$log.debug(user);
	
	return {
		//returns list of reviews
		_createService: function(){
			if(user.role.name=="editor" || user.role.name=="admin"){
				var service_url = _service_url["editor_create"];
			}else if(user.role.name=="reviewer"){
				var service_url = _service_url["reviewer"];
			}
			return service_url;
		},
		_getService:function(){
			if(user.role.name=="editor" || user.role.name=="admin"){
				var service_url = _service_url["editor_list"];
			}else if(user.role.name=="reviewer"){
				var service_url = _service_url["reviewer"];
			}else if(user.role.name=="author"){
				var service_url = _service_url["author_list"];
			}
			return service_url;
		},
		getReviews: function(article){
			var _db = Restangular.all(this._getService())
			return _db.getList({"article":article});
		},
		create: function(item){
			var _db = Restangular.all(this._createService())
			return _db.post(item, {}, {
				"X-CSRFToken": $cookies['csrftoken']
			});
		}
	};
}]);

/* Service to get article status list. */
services.factory("articleStatusList",["$rootScope", "Restangular", "$log", "$q", 
  function($rootScope,Restangular, $log, $q){
	
	var _articleStatusList = $q.defer();

	var articleStatusList = null;
	Restangular.one("statusList").get().then(function(response){
		_articleStatusList.resolve(response);
		$rootScope.articleStatusList = response;
	});
		
	return {"getArticleStatusList":function(){return articleStatusList},
	"promise": _articleStatusList.promise
	};	
}]);


/* Service to access information about article reviewers. */
services.service("articleReviewerService", ["Restangular", "$log","$cookies","currentUser",
   function(Restangular, $log, $q, $cookies, currentUser){
	
	return {
		"getReviewers": function(article){
			var _db = Restangular.one("article", article.id).one("reviewers/");
			return _db.get();
			
		}
	}
	
}]);

/*Provides servide to get all issues and create new issue */
services.service("issueService", ["Restangular", "$log", "$q", "$cookies","currentUser",
  function(Restangular, $log, $q, $cookies, currentUser){
	var URL = "issues";
	var _db = Restangular.all(URL);
	return {
		//returns list of reviews
		getIssues: function(filter){							
			return _db.getList();
		},
		getFilterdIssues: function(filter){							
			return _db.customGET("", filter);
		},
		
		getIssue: function(id){
			
			return _db.get(id);
		},
		
		create: function(issue){
			return _db.post(issue, {}, {
				"X-CSRFToken": $cookies['csrftoken']
			});
		},
		update:function(issue, queryparams, headers){
			var qp = queryparams || {};
			var headers = headers || {};
			angular.extend(headers, {
				"X-CSRFToken": $cookies['csrftoken']
			});
			return issue.put(qp, headers);
		}
	};
}]);

/*Provides servide to get all volumes and create new volumes */
services.service("volumeService", ["Restangular", "$log", "$q", "$cookies","currentUser",
  function(Restangular, $log, $q, $cookies, currentUser){
	var URL = "volumes";
	var _db = Restangular.all(URL);
	return {
		//returns list of reviews
		getVolumes: function(){
			return _db.getList();
		},
		create: function(volume){
			return _db.post(volume, {}, {
				"X-CSRFToken": $cookies['csrftoken']
			});
		},
		getIssues4Vol:function(vol){
			return _db.one(vol).getList("issues");
		}
	};
}]);

/*Provide methods to access inviteUser model*/
services.service("inviteUserService", ["Restangular", "$log", "$q", "$cookies",
  function(Restangular, $log, $q, $cookies){
	var url = "invitedusers";
	var _db = Restangular.all(url);
	return {
		create: function(item){
			return _db.post(item, {}, {
				"X-CSRFToken": $cookies['csrftoken']
			});
		}
	};
}]);


/*Provides servide to get article */
services.service("articleService", ["Restangular", "$cookies",
  function(Restangular, $cookies){
	var URL = "author/articles";
	var _db = Restangular.all(URL);
	return {
		//returns article of author		
		get: function(id){
			return _db.get(id);
		}
	};
}]);


