'use strict';
//right all the common directives here
var directives = angular.module("owjApp.directives", []);

/** directive to add ajax loader image */
directives.directive("ajaxLoader", [
function() {
	return {
		template : '<i ng-show="show" class="loadingimg"></i>',
		replace : true,
		scope : {
			show : "=",
			size : "@"
		}
	};
}]);

directives.directive("statusSelect", ["Restangular",
function(Restangular) {
	return {
		template : '<select data-ng-model="status"><option value="" selected>All</option><option data-ng-repeat="item in items" value="{{item[0]}}" selected="status==item[0]">{{item[1]}}</option></select>',

		scope : {
			status : "="
		},
		replace : true,
		link : function(scope, iElem, iAttrs) {

		},
		controller : ["$scope",
		function($scope) {
			$scope.items = Restangular.all("statusList").getList().$object;
		}]

	};
}]);

directives.directive("reviewerStatusSelect", ["Restangular",
function(Restangular) {
	return {
		template : '<select data-ng-model="status"><option value="" selected>All</option><option data-ng-repeat="item in items" value="{{item[0]}}">{{item[1]}}</option></select>',

		scope : {
			status : "="
		},
		replace : true,
		link : function(scope, iElem, iAttrs) {

		},
		controller : ["$scope",
		function($scope) {
			$scope.items = Restangular.all("reviewerStatusList").getList().$object;
		}]

	};
}]);


directives.directive("issueStatusSelect", ["Restangular",
function(Restangular) {
	return {
		template : '<select data-ng-model="published"><option value="" selected>All</option><option data-ng-repeat="item in items" value="{{item[0]}}">{{item[1]}}</option></select>',

		scope : {
			published : "="
		},
		replace : true,
		link : function(scope, iElem, iAttrs) {

		},
		controller : ["$scope",
		function($scope) {
			$scope.items = Restangular.all("issueStatusList").getList().$object;
		}]

	};
}]);





directives.directive("reviewerDateSelect", ["Restangular",
function(Restangular) {
	return {
		template : '<select data-ng-model="date"><option value="" selected>All</option><option data-ng-repeat="item in items" value="{{item[0]}}">{{item[1]}}</option></select>',

		scope : {
			date : "="
		},
		replace : true,
		link : function(scope, iElem, iAttrs) {

		},
		controller : ["$scope",
		function($scope) {
			$scope.items = Restangular.all("dateList").getList().$object;
		}]

	};
}]);

/*keeps the footer at the bottom of the page if the page height is less then the window frame*/
directives.directive("stickyFooter", [
function() {
	return {
		templateUrl : "/static/js/ngapp/tmplts/footer.html",
		replace : true,
		controller : ["$scope",
		function($scope) {

			var bannerHeight = angular.element("html.ng-scope body.owj nav#site-banner.banner.navbar.navbar-default").height();
			var navHeight = angular.element("html.ng-scope body.owj nav#main-menu.navbar.navbar-default").height();
			var contentHeight = angular.element("html.ng-scope body.owj div.ng-scope").height();
			var bodyHeight = angular.element("body").height();
			/*if(bodyHeight > (contentHeight+navHeight+bannerHeight)){
			 var footer = angular.element("html.ng-scope body.owj nav#footer.navbar.navbar-default");
			 footer.css({bottom:0, left:0, right:0, position:"fixed"});
			 }else{
			 console.log("Asdadsadasdasd");
			 }*/
		}]

	};

}]);

/*button for showing reviews article popup*/
directives.directive("reviews", [function(){
	return{
		template:'<li><a data-ng-click="viewReviews(article);" href="" tooltip="Reviews"><span class="glyphicon glyphicon-list-alt"></span></a></li>',
		replace:true,
		scope:{
			article:"=article"
		},
		controller : ["$scope", "$modal", "$log", "currentUser",
		function($scope, $modal, $log, currentUser ) {
			/*opens popup for showing reviews*/
			function viewReviews(article){
					
			    var modalInstance = $modal.open({
			      templateUrl: '/static/js/ngapp/tmplts/reviews/reviewsPopup.html',
			      controller: ["$scope", "$modalInstance", "article", "$log", "currentUser",
			      function ($scope, $modalInstance, article, $log, currentUser) {
			       $scope.article = article;
			       $scope.user = currentUser.getUser();
			        $scope.cancel = function () {
			          $modalInstance.dismiss('cancel');
			        };
			        
			      }],
			      size: "lg",
			      resolve: {
			        "article": function () {
			          return article;
			        }
			      }
			    });
			
			    modalInstance.result.then(function (selectedItem) {
			      $scope.selected = selectedItem;
			    }, function () {
			      $log.info('Modal dismissed at: ' + new Date());
			    });
			}
			$scope.viewReviews = viewReviews;

		}],
		link : function($scope, iElem, iAttrs) {
		}
	};
}]);

/*list reviews for given article*/
directives.directive("listReviews", ["$loading","$log", "currentUser", "reviewService",
  function($loading, $log, currentUser, reviewService){
	return{
		templateUrl:"listReview.html",
		replace:true,
		scope:{
			article:"="
		},
		controller: ["$scope", function($scope){
	   	$scope.noreviews = false;
	   	$scope.reviews=[];
	   	reviewService.getReviews($scope.article.id).then(function(response){
	   		$loading.finish("getReviews");
	   		$scope.noreviews = response.length ? false:true;
	   		$scope.reviews = response;
	   		$log.debug(response);
	   		
	   	});
	  }]
	}
}]);

/*list reviews for given article*/
directives.directive("writeReviews", [function(){
	return{
		templateUrl:"writeReview.html",
		replace:true,
		scope:{
			article:"="
		},
		controller : ["$scope", "$log", "currentUser", "reviewService", "$loading",
		function($scope, $log, currentUser, reviewService, $loading) {
			$log.debug("ctrl for write Reviews");
			$scope.item = {};
			$scope.alerts = [];
			$scope.formErrors = false;
			//save review to db
			$scope.submitReview = function(item) {
				$scope.formErrors = false;
				if ($scope.reviewForm.$valid) {
				  $scope.disableSubmitbutton = true;
				  $loading.start("writeReview");
					item.article = $scope.article.id;
					reviewService.create(item).then(function() {
						$log.info("form submitted succss");
						$scope.resetForm();
						addAlert({
							msg : 'Your review has been submitted.',
							type : "success"
						});
						$loading.finish("writeReview");
						$scope.disableSubmitbutton = false;
					}, function() {
						$log.error("form submitted error");
						addAlert({
              msg : 'Error Occured. Try Again!',
              type : "error"
            });
						$loading.finish("writeReview");
						$scope.disableSubmitbutton = false;
					});
				} else {
					$scope.formErrors = true;
				}

			};

			$scope.resetForm = function() {
				$scope.item = {};
			}
			function addAlert(alert) {
				$scope.alerts = [alert];
			};

			$scope.closeAlert = function(index) {
				$scope.alerts.splice(index, 1);
			};

		}]

	};
}]);

/*button for showing update article popup*/
directives.directive("updateArticleBtn", [
function() {
	return {
		template : '<li><a data-ng-click="updateArticleModal(article);" href="" tooltip="Update Article"><span class="glyphicon glyphicon-edit"></span></a></li>',
		replace : true,
		scope : {
			article : "=article"
		},
		controller : ["$scope", "$modal", "$log",
		function($scope, $modal, $log) {
			/*opens popup*/
			function updateArticleModal(article) {

				var modalInstance = $modal.open({
					templateUrl : '/static/js/ngapp/tmplts/editor/updateArticleModal.html',
					controller : ["$scope", "$modalInstance", "article", "$log",
					function($scope, $modalInstance, article, $log) {
						$scope.article = article;
						$scope.cancel= function(){
							$modalInstance.dismiss('cancel');
						};
					}],
					resolve : {
						"article" : function() {
							return article;
						}
					}
				});

				modalInstance.result.then(function(selectedItem) {
					$scope.selected = selectedItem;
				}, function() {
					$log.info('Modal dismissed at: ' + new Date());
				});
			}

			$scope.updateArticleModal = updateArticleModal;
		}],
		link : function($scope, iElem, iAttrs) {
		}
	};
}]);

/*Update form for given article*/
directives.directive("updateArticle", [
function() {
	return {
		templateUrl : "/static/js/ngapp/tmplts/editor/updateArticleForm.html",
		replace : true,
		scope : {
			article : "="
		},
		controller : ["$scope", "$log", "articleStatusList", "$upload", "SETTINGS", "$cookies","$http",
		function($scope, $log, articleStatusList, $upload, SETTINGS, $cookies,$http) {
			articleStatusList.promise.then(function(list) {
				// populate default article status list
				$scope.articleStatusList = list;
				$scope.newStatus = $scope.article.status;
			});
			$scope.alerts = [];
			$scope.articleStatusList = {};
			$scope.selectedFile = '';
			$scope.updating = false;
			
			// file select callback
			$scope.onFileSelect = function($files) {
				$scope.selectedFile = $files;
			};
			//save review to db
			$scope.updateArticle = function(item) {
				$scope.updating = true;
				var data = {
					"article_id" : $scope.article.id,
					"status" : $scope.newStatus,

				};

				if ($scope.selectedFile) {
					$scope.upload = $upload.upload({
						url : SETTINGS.ARTICLE_TYPESET_URL,
						method : "POST",
						headers : {
							'X-CSRFToken' : $cookies['csrftoken']
						},
						data : data,
						file : $scope.selectedFile,
						fileFormDataName : $scope.selectedFile.filename
					});

					$scope.upload.then(function(response) {
						// success callback for article upload
						var resp = JSON.parse(JSON.stringify(response));
						if (resp.data.status == "error") {
							$scope.errorMsg = "An error has occurred while updating.";
							$scope.updating = false;
						} else if (resp.data.status == "ok") {
							$scope.successMsg = "Article updated successfully.";
							$scope.updating = false;
						} else{
							$scope.errorMsg = "An error has occurred while updating.";
							$scope.updating = false;
						}

					}, function(response) {
						$scope.errorMsg = "An error has occurred while updating.";
						$scope.updating = false;
					}, function(evt) {
						// Math.min is to fix IE which reports 200% sometimes
						$scope.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
					});

				} else {

					$http.post(SETTINGS.ARTICLE_TYPESET_URL, $.param(data), {
						headers : {
							"Content-Type" : "application/x-www-form-urlencoded",
							"X-CSRFToken" : $cookies['csrftoken']
						}
					}).success(function(response, status, headers, config) {
						var data = JSON.parse(JSON.stringify(response));
						if (data.status == "error") {
							$scope.errorMsg = "An error has occurred while updating.";
							$scope.updating = false;
						} else if (data.status == "ok") {
							$scope.successMsg = "Article updated successfully.";
							$scope.updating = false;
						} else{
							$scope.errorMsg = "An error has occurred while updating.";
							$scope.updating = false;
						}
					}).error(function(response, status, headers, config) {
						$scope.errorMsg = "An error has occurred while updating.";
						$scope.updating = false;
					});
				}
			};

			$scope.resetForm = function() {
				$scope.item = {};
			};

			function addAlert(alert) {
				$scope.alerts = [alert];
			};

			$scope.closeAlert = function(index) {
				$scope.alerts.splice(index, 1);
			};

		}]
	}
}]);

/*button for showing reviews article popup*/
directives.directive("reviewers", [function(){
	return{
		template:'<li><a data-ng-click="viewReviewers(article);" href="" tooltip="Reviewers"><span class="glyphicon glyphicon-user"></span></a></li>',
		replace:true,
		scope:{
			article:"=article"
		},
		controller:["$scope","$modal", "$log",  function($scope, $modal, $log){
			/*opens popup for showing reviews*/
			function viewReviewers(article){
					
				$log.debug("this is working");
				
			    var modalInstance = $modal.open({
			      templateUrl: '/static/js/ngapp/tmplts/editor/reviewers_popup.html',
			      controller: ["$scope", "$modalInstance", "article", "$log", 
			      function ($scope, $modalInstance, article, $log) {
			        $scope.article = article;
			        $scope.cancel = function () {
			          $modalInstance.dismiss('cancel');
			        };
			      }],
			      resolve: {
			        "article": function () {return article;}
			      }
			    });
			
			    modalInstance.result.then(function (selectedItem) {
			      $scope.selected = selectedItem;
			    }, function () {
			      $log.info('Modal dismissed at: ' + new Date());
			    });
				
			}
			$scope.viewReviewers = viewReviewers;
			
		}]
	}
}]);


/*button for showing reviews article popup*/
directives.directive("listReviewers", [function(){
	return{
		templateUrl:'articles-reviewers.html',
		replace:true,
		scope:{
			article:"="
		},
		controller:["$scope", "articleReviewerService", "$log", "$loading",
		  function($scope, articleReviewerService, $log, $loading){
			articleReviewerService.getReviewers($scope.article).then(function(response){
				$scope.assigned_reviewers = response["assigned_reviewers"];
				$scope.suggested_reviewers = response["suggested_reviewers"];
				$loading.finish("getReviewers");
			});
			
		}]
	}
}]);

/*button for showing reviews article popup*/
directives.directive("addReviewers", [function(){
	return{
		templateUrl:'add-reviewers.html',
		replace:true,
		scope:{
			article:"="
		},
		controller:["$scope", "$http", "Restangular", "$log", "$cookies", "$loading", "inviteUserService",
		  function($scope, $http, Restangular, $log, $cookies, $loading, inviteUserService){
			$scope.usersToAdd = [];
			$scope.alerts = [];
			
			$scope.pushToReviewersList = function($item){
				$scope.usersToAdd.push($item);
			};
			
			$scope.removeUser = function(user){
				$scope.usersToAdd.splice($scope.usersToAdd.indexOf(user), 1);
			}
			
			$scope.saveUsers = function(){
				$loading.start("addReviewers");
				var db = Restangular.one("article", $scope.article.id).all("reviewers/");
				var userIds =[];
				angular.forEach($scope.usersToAdd, function(user, index){
					userIds.push(user["id"]);
				});
				db.post({"users":userIds}, {}, {
					"X-CSRFToken": $cookies['csrftoken']
				}).then(function(response){
					$loading.finish("addReviewers");
					addAlert({type:"success", msg: "Reviewers added successfully."})
				});
				
				
			}
			
			function addAlert(alert) {
		    $scope.alerts = [alert];
		  };

		  $scope.closeAlert = function(index) {
		    $scope.alerts.splice(index, 1);
		  };
			
			
			
			$scope.findReviewers = function(val) {
				$loading.start("findReviewers");
		    return $http.get('/api/v1/users/', {params: {
		        role: "reviewer",
		        q:val
		      }
		    }).then(function(res){
		    	
		    	var users = [];
		      angular.forEach(res.data, function(item){
		      	users.push(item);
		      });
		      $loading.finish("findReviewers");
		      return users;
		    });
		  };
			
		  $scope.inviteReviewerFormShowErrors= false;
		  //on submit of invite user form sends invitation to user
		  $scope.inviteReviewer = function(){
		  	if($scope.inviteUserForm.$valid){
		  		$loading.start("inviteReviewers");
		  		inviteUserService.create($scope.item).then(function(response){
		  			$scope.item = {};
			  		$scope.inviteUserForm.$setPristine();
			  		$scope.inviteReviewerFormShowErrors = false;
			  		addAlert({type:"success",msg:"Invitation Sent."})
			  		$loading.finish("inviteReviewers");
		  		}, function(response){
		  			addAlert({type:"error",
		  				msg:"Failed! Please try again after some time."});
//		  			$loading.finish("inviteReviewers");
		  		});
		  		
		  	}else{
		  		$scope.inviteReviewerFormShowErrors = true;
		  	}
		  	
		  };
		  
		}]
	
	}
}]);


//for listing most popular articles
directives.directive("popularArticles", [function(){
	return {
		templateUrl: "/static/js/ngapp/tmplts/popular_articles.html",
		controller: ["$scope", "Restangular", "$cacheFactory", function($scope, Restangular, $cacheFactory){
			var _db = Restangular.all("popular/articles/");
			var cache = $cacheFactory.get('owj');
			if(!cache){
				cache = $cacheFactory("owj");
			} 
			
			$scope.articles = cache.get("popularArticles");
			
			if(angular.isUndefined($scope.articles)){
				cache.put("popularArticles", "wait");
				_db.getList().then(function(response){
				  $scope.articles = response;
				  cache.put("popularArticles", response);
				});
			}
			
		}]
	};
}]);