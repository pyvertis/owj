'use strict';
//right all the common controllers here
var controllers = angular.module("owjApp.controllers", ["angularFileUpload"]);

/*controller for article create*/
controllers.controller("articleFormCtrl", ["$scope", "SETTINGS", "$cookies", "$upload", 
	"$modal", "$timeout", "currentUser", "$rootScope", "$log","$loading","$location",
	"articleService", "$http",
function($scope, SETTINGS, $cookies, $upload, $modal, $timeout, currentUser, $rootScope, $log,
	$loading, $location, articleService,$http) {
	$rootScope.filterTemplate ="";
	$scope.csrftoken = $cookies['csrftoken'];
	$scope.article = [];
	$scope.newAuthor = {};
	$scope.newReviewer = {};
	$scope.formErrors = false;
	$scope.customFormErrors = false;
	$scope.user = {};
	$scope.article.ext_authors = [];
	$scope.article.ext_reviewers = [];
	$scope.showAuthorAddForm = false;
	$scope.showReviewerAddForm = false;
	$scope.selectedFile = [];
	$scope.maxAuthors = 20;
	$scope.maxReviewers = 10;
	$scope.minReviewersRequired = 3;
	$scope.uploading = false;
	$scope.uploadError = false;
	$scope.edit = false;
	$scope.fileTypeError = false;
	
	
	$scope.select2Options = {
      'multiple': true,
      'simple_tags': true,
      'tags': [],  // Can be empty list.
			'containerCssClass': "select2Ui",
			'maximumSelectionSize': 5,
			'formatNoMatches': ''
  };
	
	var path = $location.absUrl();
	var articleId =  parseInt(path.substr().split('/')[5]);
		if(!isNaN(articleId)){
		articleService.get(articleId).then(function(response){
			$scope.edit = true;
			$scope.article = response;
			var key_words = [];
			$scope.select2Options["tags"] = response.key_words;
			
		},function(response){
			
			
		});
	}
	
	var userpromise = currentUser.promise.then(function(user) {
		// populate default author in form authors
		$scope.user = user;
		// only add author if we are in create mode.
		if(!$scope.articleId){
			$scope.article.ext_authors.push({
				first_name : $scope.user.first_name,
				last_name : $scope.user.last_name,
				affiliation : $scope.user.affiliation,
				email : $scope.user.email,
				is_author : true,
				delta : 1,
				id : $scope.user.id
			});
		}
	});

	$scope.addNewAuthor = function() {
		if ($scope.article.ext_authors.length >= $scope.maxAuthors) {
			return false;
		}
		$scope.showAuthorAddForm = true;
	};

	$scope.cancelNewAuthor = function() {
		$scope.showAuthorAddForm = false;
		$scope.newAuthor.first_name = "";
		$scope.newAuthor.last_name = "";
		$scope.newAuthor.affiliation = "";
		$scope.newAuthor.email = "";
	};

	$scope.saveNewAuthor = function() {
		$scope.newAuthorformErrors = false;
		
		if ($scope.articleForm.newAuthorform.$invalid) {
			$scope.newAuthorformErrors = true;
			return;
		}
		$scope.addAuthorFields($scope.newAuthor);
		$scope.newAuthor.first_name = "";
		$scope.newAuthor.last_name = "";
		$scope.newAuthor.affiliation = "";
		$scope.newAuthor.email = "";
		$scope.showAuthorAddForm = false;
	};

	// add new author field
	$scope.addAuthorFields = function(newAuthor) {
		if ( typeof $scope.article.ext_authors === 'undefined') {
			$scope.article.ext_authors = [];
		}
		$scope.article.ext_authors.push({
			first_name : newAuthor.first_name,
			last_name : newAuthor.last_name,
			affiliation : newAuthor.affiliation,
			email : newAuthor.email,
			is_author : false,
			delta : $scope.article.ext_authors.length + 1
		});
	};

	// remove author field
	$scope.removeAuthor = function(index) {
		if (!$scope.article.ext_authors[index].is_author) {
			$scope.article.ext_authors.splice(index, 1);
			$scope.updateDelta($scope.article.ext_authors);
			return true;
		}
		return false;
	};

	// reorder authors
	$scope.sortableAuthors = {
		stop : function(e, ui) {
			$scope.updateDelta($scope.article.ext_authors);
		}
	};

	$scope.addNewReviewer = function() {
		if ($scope.article.ext_reviewers.length >= $scope.maxReviewers) {
			return false;
		}
		$scope.showReviewerAddForm = true;
	};

	$scope.cancelNewReviewer = function() {
		$scope.showReviewerAddForm = false;
		$scope.newReviewer.first_name = "";
		$scope.newReviewer.last_name = "";
		$scope.newReviewer.affiliation = "";
		$scope.newReviewer.email = "";
	};

	$scope.saveNewReviewer = function() {
		$scope.newReviewerformErrors = false;
		if ($scope.articleForm.newReviewerform.$invalid) {
			$scope.newReviewerformErrors = true;
			return;
		}
		$scope.addReviewerFields($scope.newReviewer);
		$scope.newReviewer.first_name = "";
		$scope.newReviewer.last_name = "";
		$scope.newReviewer.affiliation = "";
		$scope.newReviewer.email = "";
		$scope.showReviewerAddForm = false;
	};

	// add new reviewer field
	$scope.addReviewerFields = function(newreviewer) {
		if ( typeof $scope.article.ext_reviewers === 'undefined') {
			$scope.article.ext_reviewers = [];
		}
		$scope.article.ext_reviewers.push({
			first_name : newreviewer.first_name,
			last_name : newreviewer.last_name,
			affiliation : newreviewer.affiliation,
			email : newreviewer.email,
			delta : $scope.article.ext_reviewers.length + 1
		});
	};

	// remove reviewer field
	$scope.removeReviewer = function(index) {
		$scope.article.ext_reviewers.splice(index, 1);
	};

	// update delta (order) field in authors
	$scope.updateDelta = function(iterable) {
		angular.forEach(iterable, function(value, index) {
			if(value.delta){
				value.delta = index;
			}
		});
	};

	// file select callback
	$scope.onFileSelect = function($files) {
		$scope.selectedFile = $files;
		if($scope.selectedFile.length < 1){
			$scope.fileTypeError = false;
			return;
		}
		
		if($scope.selectedFile[0].type == 'application/pdf'){
			$scope.fileTypeError = false;
			$scope.selectedFile = $files;	
		}else{
			$scope.fileTypeError = true;
		}
	};

	// article submit callback
	$scope.articleSubmit = function() {
		$scope.formErrors = false;
		$scope.customFormErrors = false;
		$scope.formErrorMsg = "";
		$scope.cancelNewAuthor();
		$scope.cancelNewReviewer();
		if ($scope.articleForm.$invalid || ! $scope.article.key_words.length) {
			$scope.formErrors = true;
			return;
		}
		if(!$scope.edit && (!$scope.selectedFile.length || $scope.fileTypeError)){
			$scope.formErrors = true;
			return;
		}		
		if (!$scope.formValid()) {
			$scope.customFormErrors = true;
			return;
		}
		$scope.uploadArticle();
	};

	$scope.formValid = function() {
		var keywordsArr = $scope.article.key_words;
		if (keywordsArr.length > 5) {
			$scope.formErrorMsg = "You can select up to 5 comma(,) separated keywords.";
			return false;
		}
		if ($scope.article.ext_authors.length >= $scope.maxAuthors) {
			$scope.formErrorMsg = "You can add upto 20 authors";
			return false;
		}
		if ($scope.article.ext_reviewers.length < $scope.minReviewersRequired) {
			$scope.formErrorMsg = "You must add atleast 3 reviewers.";
			return false;
		}
		
		if ($scope.article.ext_reviewers.length > $scope.maxReviewers) {
			$scope.formErrorMsg = "You can add upto 10 reviewers";
			return false;
		}		
		
		return true;
	};

	// reset article form
	$scope.resetForm = function() {
		$scope.formErrors = false;
		$scope.article.title = "";
		$scope.article.abstract = "";
		$scope.article.key_words = "";
		for(var i=0; i < $scope.article.ext_authors.length; i++){
			if($scope.removeAuthor(i)){
				--i;
			};
		}
		
		$scope.article.ext_reviewers= [];
		$scope.selectedFile = [];
		$scope.article.freeOfPlagiarism = false;
		$scope.article.publicationFee = false;
		$scope.article.licenseToPublish = false;
		$scope.formErrors = false;
	};

	// upload article
	$scope.uploadArticle = function() {
		$scope.uploading = true;
		$loading.start("uploadArticle");
		var rawArticle = {
			"title" : $scope.article.title,
			"abstract" : $scope.article.abstract,
			"keywords" : $scope.article.key_words,
			"authors" : $scope.article.ext_authors,
			"reviewers" : $scope.article.ext_reviewers,
		};
		if($scope.edit && $scope.article.id){
			rawArticle['id'] = $scope.article.id;
		}
		
		if($scope.selectedFile){
			$scope.upload = $upload.upload({
				url : SETTINGS.ARTICLE_FILE_UPLOAD_URL,
				method : $scope.httpMethod,
				headers : {
					'X-CSRFToken' : $cookies['csrftoken']
				},
				data : {
					"article" : rawArticle
				},
				file : $scope.selectedFile,
				fileFormDataName : $scope.selectedFile.filename
			});
	
			$scope.upload.then(function(response) {
				if(response.data.status ="ok"){
					$scope.uploadError= false;
				}else if(response.data.status ="error"){
					$scope.uploadError= true;
				}else{
					$scope.uploadError= true;
				}
				$loading.finish("uploadArticle");
				$scope.showModal($scope.uploadError);			
				$scope.uploading = false;
							
			}, function(response) {
				$scope.uploadError= true;
				$loading.finish("uploadArticle");	
				$scope.showModal($scope.uploadError);
				$scope.uploading = false;
				
			}, function(evt) {
				// Math.min is to fix IE which reports 200% sometimes
				$scope.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
			});
		} else {
				$http.post(SETTINGS.ARTICLE_FILE_UPLOAD_URL, $.param({"article" : JSON.stringify(rawArticle)}), {
					headers : {
						"Content-Type" : "application/x-www-form-urlencoded",
						"X-CSRFToken" : $cookies['csrftoken']
					}
				}).success(function(response, status, headers, config) {
					var data = JSON.parse(JSON.stringify(response));
					if (data.status == "error") {
						$scope.uploadError= true;
					} else if (data.status == "ok") {
						$scope.uploadError = false;
					} else{
						$scope.uploadError = true;
					}
					$loading.finish("uploadArticle");
					$scope.showModal($scope.uploadError);
					$scope.uploading = false;					
				}).error(function(response, status, headers, config) {
					$scope.uploadError = true;
					$loading.finish("uploadArticle");	
					$scope.showModal($scope.uploadError);
					$scope.uploading = false;					
				});
			}
		
		$scope.showModal = function(status){
			var modalInstance = $modal.open({
					templateUrl : SETTINGS.STATIC_URL + 'js/ngapp/tmplts/author/articleUploadSuccessModal.html',
					size : '',
					resolve : {"uploadError": function () {
	          						return status ;
	        				  }},
					controller : ArticleUploadSuccessCtrl
				});
		};
			
			
	};
}]);

// controller for article submit modal box
function ArticleUploadSuccessCtrl($scope, $modal, $modalInstance, $window, uploadError) {
	$scope.uploadError=uploadError;
	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
	
	$modalInstance.result.then(function (event) {
        
    	}, function (event) {
        	if(!$scope.uploadError){
				$window.location.href='/author/submissions/';
			}
    });
   
}

controllers.controller("createArtcileCtrl", ["$scope", "$rootScope",
function($scope, $rootScope) {
	//empty controller. dont delete this.

}]);

/*list authors articles*/
controllers.controller("AuthorSubmissionCtrl", ["$scope", "$log", "Restangular", "$loading", "$rootScope",
                                                "$modal","SETTINGS",
function($scope, $log, Restangular, $loading, $rootScope, $modal,SETTINGS) {
	$rootScope.filterTemplate = "/static/js/ngapp/tmplts/author/authorSubmissionFilter.html";
	$scope.orderByField = 'created';
	$scope.reverseSort = true;
	$scope.disableInfiniteScroll = false;
	$scope.noarticles = false; // to show a message when user have created no articles
	$scope.filterForm = {
		page : 1,
	};
	$scope.articles = [];
	$scope.status =  "";

	$scope.filter = function() {
		$scope.loadArticles();
	};

	$scope.loadArticles = function() {

		$loading.start("articles");

		$scope.disableInfiniteScroll = true;
		Restangular.all("author/articles/").customGET("", $scope.filterForm).then(function(response) {
			$scope.disableInfiniteScroll = false;

			if ($scope.filterForm.page == 1) {
				$scope.articles = [];
				$scope.articles = response.results;
				if ($scope.articles.length == 0) {
					$scope.noarticles = true;
				}
			} else {
				$scope.articles = $scope.articles.concat(response.results);
			}
			if (response.next && response.results.length) {
				$scope.filterForm.page = response.next;
			} else {
				$scope.disableInfiniteScroll = true;
			}
			$loading.finish("articles");
		});

	};
	$scope.loadArticles();	
	
	 $scope.makePaymentModal = function(article){
	 	if(article.paid || !article.can_author_pay){
	 		return;
	 	}
    	var modalInstance = $modal.open({
					templateUrl : SETTINGS.STATIC_URL + 'js/ngapp/tmplts/author/paymentModal.html',
					size : '',
					resolve : {"article": function () {
	          						return article ;
	        				  }},
					controller : ArticlePyamentCtrl
				});
    };
    
	
}]);

// controller for article payment modal
function ArticlePyamentCtrl($scope, $modalInstance,article,SETTINGS,$loading,$http,$cookies,$window) {
	$scope.paymentError = false;
	$scope.doPayment = false;
	$scope.redirect = false;
	$scope.article = article;
	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
	$scope.initPyament = function(article){
		$scope.doPayment = true;
		$loading.start("articlePayment");
		$http.post(SETTINGS.ARTICLE_INITPAYMENT_URL, $.param({'article_id':article.id}), {
				headers : {
					"Content-Type" : "application/x-www-form-urlencoded",
					"X-CSRFToken" : $cookies['csrftoken']
				}
			}).success(function(response, status, headers, config) {
				var data = JSON.parse(JSON.stringify(response));
				if (data.status == "error") {
					$scope.paymentError= true;
				} else if (data.status == "ok") {
					$scope.paymentError = false;
					$scope.redirect = true;
					$window.location.href = data.redirect;
				} else{
					$scope.paymentError = true;
				}
				$loading.finish("articlePayment");
				$scope.doPayment = false;					
			}).error(function(response, status, headers, config) {
				$scope.paymentError = true;
				$loading.finish("articlePayment");	
				$scope.doPayment = false;					
			});
	};
}


/*ctrl for reviewers assignment page*/
controllers.controller("reviewerAssignmentCtrl", ["$scope", "$log", "Restangular", "$loading", "$rootScope",
         "$modal",
function($scope, $log, Restangular, $loading, $rootScope, $modal) {
	$scope.orderByField = 'created';
  $scope.reverseSort = true;	
	$rootScope.filterTemplate = "/static/js/ngapp/tmplts/reviewer/reviewerAssignmentsFilter.html";
	$scope.disableInfiniteScroll = false;
	$scope.filterForm = {
		page : 1,
		status : "",
		date : ""
	};
	$scope.articles = [];

	$scope.filter = function() {
		$scope.loadReviewerArticles();
	};

	$scope.loadReviewerArticles = function() {

		$loading.start("articles");

		$scope.disableInfiniteScroll = true;
		Restangular.all("reviewer/articles/").customGET("", $scope.filterForm).then(function(response) {
			$scope.disableInfiniteScroll = false;

			if ($scope.filterForm.page == 1) {
				$scope.articles = response.results;
			} else {
				$scope.articles = $scope.articles.concat(response.results);
			}
			if (response.next && response.results.length) {
				$scope.filterForm.page = response.next;
			} else {
				$scope.disableInfiniteScroll = true;
			}
			$loading.finish("articles");
		});

	};
	$scope.loadReviewerArticles();
	
	/*opens popup for showing reviews*/
	function viewReviews(article){
			
	    var modalInstance = $modal.open({
	      templateUrl: '/static/js/ngapp/tmplts/reviewsPopup.html',
	      controller: ["$scope", "$modalInstance", "article", "$log", 
	      function ($scope, $modalInstance, article, $log) {
	       $scope.article = article;
	
	        $scope.cancel = function () {
	          $modalInstance.dismiss('cancel');
	        };
	        
	      }],
	      size: "lg",
	      resolve: {
	        "article": function () {
	          return article;
	        }
	      }
	    });
	
	    modalInstance.result.then(function (selectedItem) {
	      $scope.selected = selectedItem;
	    }, function () {
	      $log.info('Modal dismissed at: ' + new Date());
	    });
		
		
	}
	$scope.viewReviews = viewReviews;
	
}]);

controllers.controller("biddingPoolCtrl", ["$scope", "$log", "Restangular", "$loading", "$rootScope", "$modal",
function($scope, $log, Restangular, $loading, $rootScope, $modal) {
	$scope.orderByField = 'created';
	$scope.reverseSort = true;
	$rootScope.filterTemplate = "";
	$scope.disableInfiniteScroll = false;
	$scope.filterForm = {
		page : 1,
		bidding : true // this will be always true
	};
	$scope.articles = [];

	$scope.loadReviewerArticles = function() {

		$loading.start("articles");

		$scope.disableInfiniteScroll = true;
		Restangular.all("articles/").customGET("", $scope.filterForm).then(function(response) {
			$scope.disableInfiniteScroll = false;

			if ($scope.filterForm.page == 1) {
				$scope.articles = response.results;
			} else {
				$scope.articles = $scope.articles.concat(response.results);
			}
			if (response.next && response.results.length) {
				$scope.filterForm.page = response.next;
			} else {
				$scope.disableInfiniteScroll = true;
			}
			$loading.finish("articles");
		});

	};
	$scope.loadReviewerArticles();

	$scope.requestReview = function(article) {
		var modalInstance = $modal.open({
			templateUrl : '/static/js/ngapp/tmplts/reviewer/requestReviewModal.html',
			size : '',
			resolve : {
				article : function() {
					return article;
				}
			},
			controller : requestReviewModalCtrl
		});

	};

}]);

// controller for request article to review by reviewer modal box
function requestReviewModalCtrl($scope, $modal, $modalInstance, $cookies, $http, SETTINGS, article) {
	$scope.article = article;
	$scope.requesting = false;

	$scope.sendRequest = function() {
		$scope.requesting = true;
		$scope.errorMsg = "";
		$scope.infoMsg = "";
		$scope.successMsg = "";
		var postData = $.param({
			"article_id" : $scope.article.id
		});

		$http.post(SETTINGS.ARTICLE_REVIEW_REQUEST_URL, postData, {
			headers : {
				"Content-Type" : "application/x-www-form-urlencoded",
				"X-CSRFToken" : $cookies['csrftoken']
			}
		}).success(function(response, status, headers, config) {
			var data = JSON.parse(JSON.stringify(response));
			if (data.status == "error") {
				$scope.errorMsg = "An error has occurred while requesting.";
				$scope.requesting = false;
			} else if (data.status == "requested") {
				$scope.successMsg = "Your reques has been sent.";
				$scope.requesting = false;
			} else if (data.status == "exist") {
				$scope.infoMsg = "Your have already requested.";
				$scope.requesting = false;
			}
		}).error(function(response, status, headers, config) {
			$scope.errorMsg = "An error has occurred while requesting.";
			$scope.requesting = false;
		});
	};

	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
}

/*ctrl to list all articles for editors*/
controllers.controller("edtiorArticlesCtrl", ["$scope", "$log", "Restangular", "$loading", "$rootScope",
                                              "$modal", "DJUrls", 
function($scope, $log, Restangular, $loading, $rootScope, $modal, DJUrls) {
	$scope.Urls = DJUrls;
	$scope.orderByField = 'created';
  	$scope.reverseSort = true;	
	$rootScope.filterTemplate = "/static/js/ngapp/tmplts/editor/editorArticlesFilter.html";
	$scope.disableInfiniteScroll = false;
	$scope.noarticles = false;
	// to show a message when user have created no articles
	$scope.filterForm = {
		page : 1,
		status : "",
		date : ""
	};
	$scope.articles = [];

	$scope.filter = function() {
		$scope.loadArticles();
	};

	$scope.loadArticles = function() {
		$loading.start("editorarticles");
		$scope.disableInfiniteScroll = true;
		Restangular.all("editor/articles/").customGET("", $scope.filterForm).then(function(response) {
			$scope.disableInfiniteScroll = false;

			if ($scope.filterForm.page == 1) {
				$scope.articles = [];
				$scope.articles = response.results;
				if ($scope.articles.length == 0) {
					$scope.noarticles = true;
				}
			} else {
				$scope.articles = $scope.articles.concat(response.results);
			}
			if (response.next && response.results.length) {
				$scope.filterForm.page = response.next;
			} else {
				$scope.disableInfiniteScroll = true;
			}
			$loading.finish("editorarticles");
		});

	};
	$scope.loadArticles();
	
	$scope.moveToBiddingPool = function(article){
			var modalInstance = $modal.open({
			templateUrl : '/static/js/ngapp/tmplts/editor/moveToBiddingPoolModal.html',
			size : '',
			resolve : {
				article : function() {
					return article;
				}
			},
			controller : moveToBiddingPoolCtrl
		});
	};

}]);


// controller for article to move in bidding pool by editor modal box
function moveToBiddingPoolCtrl($scope, $modal, $modalInstance, $cookies, $http, SETTINGS, article) {
	$scope.article = article;
	$scope.requesting = false;

	$scope.move = function() {
		$scope.requesting = true;
		$scope.errorMsg = "";
		$scope.successMsg = "";
		$scope.infoMsg ="";
		var postData = $.param({
			"article_id" : $scope.article.id
		});

		$http.post(SETTINGS.ARTICLE_MOVE_BIDDING_POOL_URL, postData, {
			headers : {
				"Content-Type" : "application/x-www-form-urlencoded",
				"X-CSRFToken" : $cookies['csrftoken']
			}
		}).success(function(response, status, headers, config) {
			var data = JSON.parse(JSON.stringify(response));
			if (data.status == "error") {
				$scope.errorMsg = "An error has occurred while moving article to bidding pool.";
				$scope.requesting = false;
			} else if (data.status == "ok") {
				$scope.successMsg = "Article has moved to bidding pool.";
				$scope.requesting = false;
			}else if (data.status == "exist") {
				$scope.infoMsg = "Article already in bidding pool";
				$scope.requesting = false;
			} else{
				$scope.errorMsg = "An error has occurred while moving article to bidding pool.";
				$scope.requesting = false;
			}
		}).error(function(response, status, headers, config) {
			$scope.errorMsg = "An error has occurred while moving article to bidding pool.";
			$scope.requesting = false;
		});

	};

	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
}

/*Editor build issue page controller */
controllers.controller("buildIssueCtrl", ["$scope", "$log","$modal","issueService","Restangular",
"$cookies","$loading","SETTINGS","DJUrls",
function($scope, $log, $modal,issueService,Restangular,$cookies,$loading,SETTINGS, DJUrls) {
	$scope.orderByField = 'created';
	$scope.reverseSort = true;
	$scope.saving = false;
	$scope.loading = false;
	$scope.dirty = false;	
	$scope.issueUpdateError = false;
	$scope.issues=[];
	$scope.readyArticles=[];
	$scope.previousreadyArticles=[];
	$scope.selectedIssue = {};
	$scope.previousselectedIssue = {};
	issueService.getIssues().then(function(response){
   		$scope.issues = response;   	
   		$scope.selectedIssue = $scope.issues[0];
   		angular.copy($scope.selectedIssue,$scope.previousselectedIssue);
   	});
	$scope.Urls = DJUrls;
	
	$scope.filterForm = {
		page:1,
		status : ["paid","accepted","published","typeset"],
		notinissue : true		
	};	
	
	$scope.loadArticles = function() {
		// disable loading of article while saving selected article
		if(!$scope.saving){
			$scope.loading = true;
			$scope.disableInfiniteScroll = true;
			Restangular.all("editor/articles/").customGET("", $scope.filterForm).then(function(response) {
				$scope.disableInfiniteScroll = false;
	
				if ($scope.filterForm.page == 1) {
					$scope.readyArticles = [];
					$scope.readyArticles = response.results;
					angular.copy($scope.readyArticles,$scope.previousreadyArticles);
					
					if ($scope.readyArticles.length == 0) {
						$scope.noarticles = true;
					}
				} else {
					$scope.readyArticles = $scope.readyArticles.concat(response.results);
					angular.copy($scope.readyArticles,$scope.previousreadyArticles);
				}
				if (response.next && response.results.length) {
					$scope.filterForm.page = response.next;
				} else {
					$scope.disableInfiniteScroll = true;
				}
				$scope.loading= false;
			},
			function(response){
				$scope.loading= false;				
				}
			);
		}
	};
	$scope.loadArticles();
	
	$scope.add = function(article){		
		$scope.dirty = true;
		var index = 0;
		for(var i=0; i < $scope.readyArticles.length ; i++) {
		 	if($scope.readyArticles[i].id == article.id) {
 				index = i;
          		break;
          	} 
		}
		
		$scope.selectedIssue.articles.push(article);
		$scope.readyArticles.splice(index, 1);		
	};
	
	$scope.remove = function(article){
		$scope.dirty = true;
		var index = 0;
		for(var i=0; i < $scope.selectedIssue.articles.length ; i++) {
		 	if($scope.selectedIssue.articles[i].id == article.id) {
 				index = i;
          		break;
          	} 
		}
		
		$scope.readyArticles.push(article);
		$scope.selectedIssue.articles.splice(index, 1);
		$scope.noarticles = false;		
	};

	
	$scope.discard = function(){
		angular.copy($scope.previousselectedIssue,$scope.selectedIssue);
		angular.copy($scope.previousreadyArticles,$scope.readyArticles);
		$scope.dirty = false;
	};	
	
	$scope.selectedIssueChanged = function(){
		angular.copy($scope.selectedIssue,$scope.previousselectedIssue);
	};
	
	$scope.saveIssue = function(){
		$loading.start('buildissue');		
		$scope.saving = true;
		var articleIds = [];
		for (var i = $scope.selectedIssue.articles.length - 1; i >= 0; i--){
		  articleIds.push($scope.selectedIssue.articles[i].id);
		};
		
		//use the below which is close to rest semantics
		$scope.selectedIssue.articles_ids = articleIds;
		issueService.update($scope.selectedIssue).then(function(response){
			angular.copy($scope.selectedIssue,$scope.previousselectedIssue);
			angular.copy($scope.readyArticles,$scope.previousreadyArticles);
			$scope.saving = false;
			$scope.dirty = false;
			$scope.issueUpdateError = false;
			$loading.finish('buildissue');
			var modalInstance = $modal.open({
				templateUrl : SETTINGS.STATIC_URL + 'js/ngapp/tmplts/editor/updateIssueModalMsg.html',
				resolve : {"issueUpdateError": function () {
          						return $scope.issueUpdateError ;
        				  }},
				controller : issueUpdateModalMsgCtrl
			});
			
		},function(response){
			$scope.saving = false;
			$loading.finish('buildissue');
			$scope.issueUpdateError = true;
			var modalInstance = $modal.open({
				templateUrl : SETTINGS.STATIC_URL + 'js/ngapp/tmplts/editor/updateIssueModalMsg.html',
				resolve : {"issueUpdateError": function () {
          						return $scope.issueUpdateError ;
        				  }},
				controller : issueUpdateModalMsgCtrl
			});		
		});	
	};
		
	$scope.createIssueModal = function(){
			var modalInstance = $modal.open({
			templateUrl : '/static/js/ngapp/tmplts/editor/buildIssueModal.html',
			resolve : {},
			controller : ["$scope","$modalInstance",
							function($scope,$modalInstance) {
								$scope.cancel = function() {
									$modalInstance.dismiss('cancel');		
								};
							}]
		});
	};
	
	$scope.createVolumeModal = function(){
			var modalInstance = $modal.open({
			templateUrl : '/static/js/ngapp/tmplts/editor/createVolumeModal.html',
			size : 'sm',
			resolve : {},
			controller : ["$scope","$modalInstance",
							function($scope,$modalInstance) {
								$scope.cancel = function() {
									$modalInstance.dismiss('cancel');		
								};
							}]
		});
	};
	
}]);

// controller for issue update msg modal box
function issueUpdateModalMsgCtrl($scope, $modal, $modalInstance,issueUpdateError) {
	$scope.issueUpdateError=issueUpdateError;
	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');		
	};
}


/* Editor page create issue form controller */
controllers.controller("createIssueFormCtrl", ["$scope", "SETTINGS", "$cookies", "volumeService",
"$http","issueService",
function($scope, SETTINGS, $cookies, volumeService, $http,issueService) {
	$scope.issue ={};
	$scope.formErrors= false;
	$scope.creating = false;
	$scope.volumeList = '';
	$scope.datePicker ={opened:false};
	$scope.publishDate = null;
    $scope.dateOptions = {
		startingDay: 1
	};
    $scope.successMsg="";
    $scope.errorMsg="";
    
    volumeService.getVolumes().then(function(response){
   		// populates volume list
   		$scope.volumeList = response;
   	});
	
	$scope.today = function() {
    	$scope.issue.publishDate = new Date();
  	};
  	
  	$scope.today();
	
	$scope.clear = function () {
    	$scope.issue.publishDate = null;
  	};
  	
  	$scope.open = function($event) {
		$event.preventDefault();
	    $event.stopPropagation();
	    $scope.datePicker.opened = !$scope.datePicker.opened;	    
  	};  	
    
    $scope.createIssue = function(){
    	if ($scope.createIssueForm.$invalid) {
			$scope.formErrors = true;
			return;
		}
		
		var publishDate = $scope.issue.publishDate.getFullYear()+'-'+
									($scope.issue.publishDate.getMonth()+1)+'-'+
									$scope.issue.publishDate.getDate();
		var issue = {title:$scope.issue.title,
					volume:$scope.issue.volume,
					issue_number:$scope.issue.issueNo,
					publish_date:publishDate
					};
					
		$scope.creating = true;
		
		issueService.create(issue).then(function() {
				$scope.successMsg = "Issue created successfully.";
				$scope.creating = false;
			}, function() {
				$scope.errorMsg = "An error has occurred while creating issue.";
				$scope.creating = false;
			});
    };
}]);


/* Editor page create volume form controller */
controllers.controller("createVolumeFormCtrl", ["$scope", "SETTINGS", "$cookies", 
"$http","volumeService",
function($scope, SETTINGS, $cookies, $http,volumeService) {
	$scope.volume ={};
	$scope.formErrors= false;
	$scope.creating = false;
	$scope.successMsg="";
    $scope.errorMsg="";
    
    $scope.createVolume = function(){
    	if ($scope.createVolumeForm.$invalid) {
			$scope.formErrors = true;
			return;
		}
					
		$scope.creating = true;
		
		volumeService.create($scope.volume).then(function() {
				$scope.successMsg = "Volume created successfully.";
				$scope.creating = false;
			}, function() {
				$scope.errorMsg = "An error has occurred while creating volume.";
				$scope.creating = false;
			});
    };
}]);

/* Editor publish issue page controller */
controllers.controller("publishIssueCtrl", ["$scope", "$log","$modal","issueService", "DJUrls",
function($scope, $log, $modal,issueService, DJUrls) {
	$scope.Urls = DJUrls;
	$scope.filterTemplate = "/static/js/ngapp/tmplts/issueFilter.html";
	$scope.issues = [];
	$scope.filterForm = {
		page:1,
		published : ""
	};	
	
	$scope.loadissues = function(){
		issueService.getFilterdIssues($scope.filterForm).then(function(response){
	   		$scope.issues = response;
	   		if ($scope.issues.length == 0) {
				$scope.noissues = true;
			}
	   	});   	
  };
  $scope.loadissues();
  $scope.filter = function(){
   		$scope.loadissues();	
  };
   	
   	$scope.publishIssueModal = function (issue){
   		var modalInstance = $modal.open({
		templateUrl : '/static/js/ngapp/tmplts/editor/publishIssueModal.html',
		resolve : {"issue": function () {
          						return issue ;
        				  }},
		controller : publishIssueModalCtrl
		});
   	};
}]);

/* Editor publish issue modal form controller */
function publishIssueModalCtrl($scope,$modalInstance, $cookies, issueService,issue,$timeout) {
	$scope.issue =issue;
	$scope.form= {};
	$scope.newIssue ={
		'publish_date':$scope.issue.publish_date,		
	};	
	$scope.publishing = false;
	$scope.successMsg="";
    $scope.errorMsg="";
    $scope.datePicker ={opened:false};
    
    $scope.cancel = function() {
		$modalInstance.dismiss('cancel');		
	};
	
	$scope.clear = function () {
		$scope.issue.publishDate = null;
  	};
  	
  	$scope.open = function($event) {
		$event.preventDefault();
	    $event.stopPropagation();
	    $scope.datePicker.opened = !$scope.datePicker.opened;	    
  	};
	
	$scope.today = function() {
		$timeout(function(){
			if($scope.newIssue.publish_now){ 	
		 		$scope.newIssue.publish_date = new Date();
		 	}
		 });    	
  	};
	
    $scope.publishIssue = function(){
    	if ($scope.form.publishIssueForm.$invalid) {
			$scope.formErrors = true;
			return;
		}
					
		$scope.publishing = true;
		
		issueService.getIssue($scope.issue.id).then(function(response){
   			var issues = response;
   			var publishDate = $scope.newIssue.publish_date.getFullYear()+'-'+
									($scope.newIssue.publish_date.getMonth()+1)+'-'+
									$scope.newIssue.publish_date.getDate();
   			issues.publish_date = publishDate;
   			var query = {};
   			if ($scope.newIssue.publish_now){
   				issues.is_published = $scope.newIssue.publish_now;	
   				query = {'trigger_article_publish':true};
   			}
   			
   			issueService.update(issues,query).then(function() {
				$scope.successMsg = "Issue published successfully.";
				$scope.publishing = false;
			}, function() {
				$scope.errorMsg = "An error has occurred while publishing issue.";
				$scope.publishing = false;
			});			
   		});		
	};
};

controllers.controller("filtebarCtrl", ["$scope", "$rootScope",
function($scope, $rootScope) {

}]);

/*controller for Registration form*/
controllers.controller("RegistrationCtrl", ["$scope", "$loading", "$timeout",
function($scope, $loading, $timeout) {
	function randomNumber(min, max) {
		min = min || 1;
		max = max || 10;
		return (Math.floor(Math.random() * (max - min + 1)) + min);
	}

	function setCaptcha(selector) {
		var captchaDiv = $(selector);
		captchaDiv.find("#cap_num_1").text(randomNumber());
		captchaDiv.find("#cap_num_2").text(randomNumber());
	}


	$scope.captchaValidation = function($event) {
		$loading.start("registration");
		var captchaDiv = $("#contact-captcha_control");
		captchaDiv.find("#captcha_error").hide(300);
		captchaDiv.removeClass("error");
		var userInput = captchaDiv.find("input#captcha").val();
		var int_1 = parseInt(captchaDiv.find("#cap_num_1").text());
		var int_2 = parseInt(captchaDiv.find("#cap_num_2").text());
		var sum = int_1 + int_2;
		if (userInput == sum) {
			return true;
		} else {
			captchaDiv.addClass("error");
			captchaDiv.find("#captcha_error").show(300);
			$event.preventDefault();
			$loading.finish("registration");
			return false;
		}
	};

	setCaptcha("#contact-captcha_control");

}]);


//controller which list all the articles in a issue on main article page
controllers.controller("articleListCtrl", ["$scope", "$modal","$log", "volumeService",
  function($scope, $modal, $log, volumeService) {
	$scope.searchForm = {};
	
	volumeService.getVolumes().then(function(response){
		$scope.volumes = response;
	});

	$scope.setIssues = function(){
		volumeService.getIssues4Vol($scope.searchForm.volume).then(function(response){
			$scope.issues4SelectedVol = response;
			$log.debug(response);
		});
	};
	
	$scope.resetForm = function(){
		$scope.searchForm = {};
	}
	
	//pop to show article information like abstract or citations
	$scope.popUp = function(tab, article_no){
		
			var article=$scope.articles[article_no];

			var modalInstance = $modal.open({
	    templateUrl: '/static/js/ngapp/tmplts/article-info-pop.html',
	    controller: ["$scope", "$modalInstance", 'article', "current_tab", "$log", 
	    function ($scope, $modalInstance, article, current_tab, $log) {
	    	$scope.article = article;
	    	$scope.current_tab = current_tab;
	      $scope.cancel = function () {
	        $modalInstance.dismiss('cancel');
	      };
	      
	    }],
	    resolve: {
        "article": function () {
          return article;
        },
        "current_tab":function(){return tab;}
      }
	  });

	  modalInstance.result.then(function (selectedItem) {
	    $scope.selected = selectedItem;
	  }, function () {
	    $log.info('Modal dismissed at: ' + new Date());
	  });
	}

}]);

