'use strict';
//starting point of angular application
// all the angular configurations will be maintained here

var owjApp = angular.module("owjApp", ["owjApp.controllers", "owjApp.directives", 
    "owjApp.services", 
    "ui.router", "restangular","ui.sortable", "infinite-scroll", "darthwade.dwLoading",
    "ui.bootstrap", "ngCookies", "ui.select2"]);


owjApp.constant("SETTINGS", {
	"STATIC_URL": "/static/",
	"ARTICLE_FILE_UPLOAD_URL": "/author/submit-paper/",
	"ARTICLE_REVIEW_REQUEST_URL": "/reviewer/article/request/",
	"ARTICLE_MOVE_BIDDING_POOL_URL":"/editor/article/movebiddingpool/",
	"ARTICLE_TYPESET_URL":"/editor/article/typeset/",
	"ARTICLE_INITPAYMENT_URL":"/author/article/initpayment/"	
});


owjApp.config(["$stateProvider", "$urlRouterProvider", "RestangularProvider", "$locationProvider",
	
  function($stateProvider, $urlRouterProvider, RestangularProvider, $locationProvider) {

	RestangularProvider.setBaseUrl('/api/v1');
	RestangularProvider.setRequestSuffix("/")
	
//	$urlRouterProvider.otherwise("/questionList");
	/*
	$stateProvider.
		state('author_submission', {
			url: "/submission",
			templateUrl: '/static/js/ngapp/tmplts/author/mysubmissions.html',
			controller: 'AuthorSubmissionCtrl',
	    }).
    state('author_createarticle', {
			url: "/artcile",
			templateUrl: '/static/js/ngapp/tmplts/author/create_article.html',
			controller: 'createArtcileCtrl',
	    }).
    state('reviewer_assignments', {
			url: "/assignments",
			templateUrl: '/static/js/ngapp/tmplts/reviewer/assignments.html',
			controller: 'reviewerAssignmentCtrl',
	    }).
	  state('biddingpool', {
			url: "/bidding",
			templateUrl: '/static/js/ngapp/tmplts/reviewer/biddingpool.html',
			controller: 'biddingPoolCtrl',
	    }).
    state('editor_articles', {
			url: "/articles",
			templateUrl: '/static/js/ngapp/tmplts/editor/editor_articles.html',
			controller: 'edtiorArticlesCtrl',
	    }).
    state('buildIssue', {
			url: "/buildissue",
			templateUrl: '/static/js/ngapp/tmplts/editor/build_issue.html',
			controller: 'buildIssueCtrl',
	    }).
    state('publishIssue', {
			url: "/publishissue",
			templateUrl: '/static/js/ngapp/tmplts/editor/publish_issue.html',
			controller: 'publishIssueCtrl',
	    });
		*/
}]);


owjApp.run(["currentUser", function(currentUser){
	
}]);
