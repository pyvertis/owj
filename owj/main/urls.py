from django.conf.urls import patterns, url, include
from main.views import OwjHome, Articles, Author, Reviewer, Editor, IssueView, \
    ArticleSearchView, UserInfo, ActivateUser


urlpatterns = patterns('',


    url(r'^$', OwjHome.as_view(), name='home'),

    url(r'^articles/$', Articles.as_view(), name='articles'),
    url(r'^issue/(?P<slug>[\w-]+)/$', IssueView.as_view(), name='issue_view'),
    url(r'^search/$', ArticleSearchView.as_view(), name='issue_search_view'),
    url(r'^author/submissions/$', Author.as_view(), {"view":"author_submissions"}, name='author_submissions'),
    url(r'^author/artcile/$', Author.as_view(), {"view":"create_article"}, name='create_article'),
    url(r'^author/artcile/(?P<id>\d+)/$', Author.as_view(), {"view":"create_article"}, name='edit_article'),
    url(r'^reviewer/assignments/$', Reviewer.as_view(), {"view":"reviewer_assignments"}, name='reviewer_assignments'),
    url(r'^reviewer/biddingpool/$', Reviewer.as_view(), {"view":"reviewer_biddingpool"}, name='reviewer_biddingpool'),
    url(r'^editor/$', Editor.as_view(), name='editor_articles'),
    url(r'^editor/buildissue/$', Editor.as_view(), name='editor_buildissue'),
    url(r'^editor/publishissue/$', Editor.as_view(), name='editor_publishissue'),
    
    url(r'^user/info/(?P<pk>\d+)/$', UserInfo.as_view(), name='user_info'),
    url(r'^user/activate/(?P<pk>\d+)/$', ActivateUser.as_view(), {"active": True}, name='user_activate'),
    url(r'^user/deactivate/(?P<pk>\d+)/$', ActivateUser.as_view(), {"active": False}, name='user_deactivate')
)
