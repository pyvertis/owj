from articles.models import Issue, Article
from django.conf import settings
from django.contrib.auth.decorators import login_required, permission_required
from django.core.urlresolvers import reverse, reverse_lazy
from django.shortcuts import render, get_object_or_404
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.generic import ListView
from django.views.generic.base import View, TemplateView, RedirectView
from django.views.generic.detail import DetailView
from django.db.models import Q
from django.contrib.auth import get_user_model
from django.contrib import messages
from dbmail.mail import DbSendMail
from django.contrib.sites.models import RequestSite
from owj.utils import reverse_url_with_hostname

User = get_user_model()

# Create your views here.
class OwjHome(TemplateView):
    """
    Renders the home page
    """
    template_name = "_layout/home.html"

    def get_context_data(self, **kwargs):
        context = TemplateView.get_context_data(self, **kwargs)
        context["active_nav"] = "home"
        return context


class Articles(RedirectView):
    """
    View for Articles in Main nav.
    This will simply redirect to latest Issue page.
    Also if 'issue' key is found in request.GET, will redirect to corrosponding issue
    """
    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        if self.request.REQUEST.has_key("issue_number"):
            try:
                issue = Issue.objects.get(id=self.request.REQUEST['issue'])
                url = reverse("issue_view", args=(issue.slug,))
            except:
                url = reverse("home")
        else:
            try:
                obj = Issue.objects.filter(is_published=True).latest("publish_date")
                url = reverse("issue_view", args=(obj.slug,))
            except:
                url = reverse("home")
            
        return  url


class IssueView(DetailView):
    """
    Renders a issue detail page
    """
    template_name = "main/issue.html"
    model = Issue
    
    def get_context_data(self, **kwargs):
        context = DetailView.get_context_data(self, **kwargs)
        context["active_nav"] = "article"
        return context
    
    def get_queryset(self):
        queryset = DetailView.get_queryset(self)
        return queryset.filter(is_published=True)   
     

class ArticleSearchView(ListView):
    """
    List search results for Articles Search
    """
    template_name = "main/search.html"
    model = Article
    paginate_by = 20
    
    def get_queryset(self):
        queryset = ListView.get_queryset(self)
        volume = self.request.GET.get("volume", "")
        issue = self.request.GET.get("issue", "")
        search = self.request.GET.get("search", "")
        author = self.request.GET.get("author", "")
        
        queryset = queryset.filter(status="published")  
        
        if issue:
            issues = Issue.objects.filter(id = issue).values_list("articles")
        elif volume:
            issues = Issue.objects.filter(volume = volume).values_list("articles")
        else:
            issues = []
        
        if issue:
            queryset = queryset.filter(
                            id__in = issues
                            )
            
        if author:
            firstnameQ = Q(ext_authors__first_name__icontains=author)
            lastnameQ = Q(ext_authors__last_name__icontains=author)
            emailQ = Q(ext_authors__email__icontains=author)
            queryset = queryset.filter(firstnameQ | lastnameQ | emailQ)
        
        if search:
            abstractQ = Q(abstract__icontains = search)
            titleQ = Q(title__icontains = search)
            tagQ = Q(key_words__name__icontains = search)
            queryset = queryset.filter(abstractQ | titleQ | tagQ)
        
        return queryset


class Author(TemplateView):
    """
    Renders the home page
    """
    template_name = "main/author.html"

    @method_decorator(ensure_csrf_cookie)
    @method_decorator(login_required(login_url=reverse_lazy("owj_login")))
    def dispatch(self, request, *args, **kwargs):
        return TemplateView.dispatch(self, request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = TemplateView.get_context_data(self, **kwargs)
        context["active_nav"] = "author"
        view = kwargs.get("view", "author_submissions")
        ng_template = {"author_submissions": '{}js/ngapp/tmplts/author/mysubmissions.html'.format(settings.STATIC_URL),
                       "create_article": '{}js/ngapp/tmplts/author/create_article.html'.format(settings.STATIC_URL)}

        ng_controller = {"author_submissions": 'AuthorSubmissionCtrl',
                       "create_article": 'createArtcileCtrl'}

        context["ng_template"] = ng_template[view]
        context["ng_controller"] = ng_controller[view]
        return context


class Reviewer(TemplateView):
    """
    Renders the home page
    """
    template_name = "main/reviewer.html"

    @method_decorator(ensure_csrf_cookie)
    @method_decorator(login_required(login_url=reverse_lazy("owj_login")))
    def dispatch(self, request, *args, **kwargs):
        return TemplateView.dispatch(self, request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = TemplateView.get_context_data(self, **kwargs)
        context["active_nav"] = "reviewer"
        ng_template = {reverse("reviewer_assignments"): '{}js/ngapp/tmplts/reviewer/assignments.html'.format(settings.STATIC_URL),
                       reverse("reviewer_biddingpool"): '{}js/ngapp/tmplts/reviewer/biddingpool.html'.format(settings.STATIC_URL)}

        ng_controller = {reverse("reviewer_assignments"): 'reviewerAssignmentCtrl',
                       reverse("reviewer_biddingpool"): 'biddingPoolCtrl'}

        context["ng_template"] = ng_template[self.request.path]
        context["ng_controller"] = ng_controller[self.request.path]
        return context


class Editor(TemplateView):
    """
    Renders the home page
    """
    template_name = "main/editor.html"

    @method_decorator(ensure_csrf_cookie)
    @method_decorator(login_required(login_url=reverse_lazy('owj_login')))
    def dispatch(self, *args, **kwargs):
        return super(TemplateView, self).dispatch(*args, **kwargs)


    def get_context_data(self, **kwargs):
        context = TemplateView.get_context_data(self, **kwargs)
        context["active_nav"] = "editor"
        ng_template = {reverse("editor_articles"): '{}js/ngapp/tmplts/editor/editor_articles.html'.format(settings.STATIC_URL),
                       reverse("editor_buildissue"): '{}js/ngapp/tmplts/editor/build_issue.html'.format(settings.STATIC_URL),
                       reverse("editor_publishissue"): '{}js/ngapp/tmplts/editor/publish_issue.html'.format(settings.STATIC_URL)}

        context["ng_template"] = ng_template[self.request.path]

        return context
    

class UserInfo(DetailView):
    """
    Renders a page with user information.
    This will be used by editors and admin to activate-deactivate users
    """
    template_name = "main/userinfo.html"
    model = User



class ActivateUser(RedirectView):
    """
    View to activate and deactivate a user.
    Only editors and admins are allowed to perform this action.
    """
    permanent = False
    
    @method_decorator(login_required(login_url=reverse_lazy('owj_login')))
    @method_decorator(permission_required("main.can_activate_user", login_url=reverse_lazy('owj_login')))
    def dispatch(self, request, *args, **kwargs):
        return RedirectView.dispatch(self, request, *args, **kwargs)
    
    def get(self, request, *args, **kwargs):
        """
        activate the user and send back to the page user was coming from
        """
        user_id = kwargs['pk']
        is_active = kwargs['active']
        user = get_object_or_404(User, id=user_id)

        user.is_active = is_active 
        user.save()
        value = "Activated" if user.is_active else "Deactivated"
        messages.info(request, "User {}".format(value))

        mail = DbSendMail("UAAA")

        site = RequestSite(request)
        mail_context = dict(name=user.get_full_name(),
                            status=value,
                            login_url=reverse_url_with_hostname(site, "owj_login"))
        mail.sendmail([user.email], mail_context)
        
        return RedirectView.get(self, request, *args, **kwargs) 
    
    def get_redirect_url(self, *args, **kwargs):
        return reverse("user_info", args=(kwargs['pk'],))