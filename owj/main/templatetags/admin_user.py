from django import template
from django.core.urlresolvers import reverse, reverse_lazy
register = template.Library()

@register.inclusion_tag('admin/main/user/t4_account_activation_button.html', takes_context=True)
def t4activationbutton(context):
    user = context['original']
    urlname = 'admin:admin_deactivate_user' if user.is_active else 'admin:admin_activate_user'
    return dict(title="Deactivate" if user.is_active else "Activate",
                activationurl=reverse(urlname, args=(user.pk,))
                )