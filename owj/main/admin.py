from django.contrib import admin, messages
from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from dbmail.mail import DbSendMail
from django.conf import settings
from random import choice
from string import ascii_lowercase, digits
from main.models import Role
from django.contrib.auth.models import Permission
import logging
from django.shortcuts import get_object_or_404, redirect
from django.contrib.sites.models import RequestSite
from owj.utils import reverse_url_with_hostname
from django.core.urlresolvers import reverse
from django.conf.urls import url, patterns
from owj.admin import admin_site
from cms.models.pagemodel import Page
from cms.admin.pageadmin import PageAdmin
from dbmail.admin import MailTemplateAdmin
from dbmail.models import MailTemplate
logger = logging.getLogger("owj")

User = get_user_model()

def generate_random_username(length=30, chars=ascii_lowercase + digits, split=0, delimiter='-'):
    '''
    generates random user name
    '''
    username = ''.join([choice(chars) for i in xrange(length)])
    if split:
        username = delimiter.join([username[start:start + split] for start in range(0, len(username), split)])
    try:
        User.objects.get(username=username)
        return generate_random_username(length=length, chars=chars, split=split, delimiter=delimiter)
    except User.DoesNotExist:
        return username;


class OwjAdminUserCreationForm(forms.ModelForm):
    '''
    Form to create user from django admin.
    '''
    first_name = forms.CharField(widget=forms.TextInput(attrs={'size':50}),
                                 label="First name")
    last_name = forms.CharField(widget=forms.TextInput(attrs={'size':50}),
                                label="Last name")

    email = forms.EmailField(widget=forms.TextInput(attrs={'size':50}),
                             label="Email address")
    
    def clean_email(self):
        email = self.cleaned_data['email']
        existing = User.objects.filter(email__iexact=email)
        if existing.exists():
            raise forms.ValidationError("A user with same email already exists.")
        else:
            return email

    def clean_first_name(self):
        if not self.cleaned_data['first_name']:
            raise forms.ValidationError("This field is required.")
        else:
            return self.cleaned_data['first_name']

    def clean_last_name(self):
        if not self.cleaned_data['last_name']:
            raise forms.ValidationError("This field is required.")
        else:
            return self.cleaned_data['last_name']

    def save(self, commit=True):
        '''
        create(save) user from django admin.
        '''
        new_pass = User.objects.make_random_password(length=6)
        user = super(OwjAdminUserCreationForm, self).save(commit=False)
        owj_default_group = hasattr(settings, "OWJ_DEFAULT_GROUP") and settings.OWJ_DEFAULT_GROUP or "Author"

        if not user.username:
            user.username = generate_random_username(length=8)

        if not user.role:
            # if role is not specified then assign default role
            logger.warning("Role not specified, assigning default role: {}".format(owj_default_group))
            user.role = Role.objects.get(name=owj_default_group)

        user.set_password(new_pass)

        user.save()
        self.send_email(user, new_pass)
        return user

    def send_email(self, user, new_pass):
        '''
        Notify user about account creation.
        '''
        if user.role.name.lower() == "admin" or  user.role.name.lower() == "editor":
            mail = DbSendMail("UACM")
            context = {"name":user.get_full_name(),
                       "username": user.email,
                      "password":new_pass,
                      "role":user.role.name
                      }
            mail.sendmail([user.email], context)


class OwjUserAdmin(UserAdmin):
    add_fieldsets = (
        ('Personal info', {'fields': ('first_name', 'last_name', 'email', 'about_me', 'affiliation')}),
        ('Permissions', {'fields': ('role', 'is_active', 'is_staff', 'is_superuser',
                                       'groups')}),
    )
    
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (('Personal info'), {'fields': ('first_name', 'last_name', 'email', 'about_me', 'affiliation')}),
        (('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )

    list_display = ("get_full_name", "email", "role", "is_active")
    add_form = OwjAdminUserCreationForm

    def activate_user_view(self, request, **kwargs):
        """
        Activate/Deactivate the user and redirect to change page.
        """
        user_id = kwargs['userpk']
        is_active = kwargs['active']
        user = get_object_or_404(User, id=user_id)

        user.is_active = is_active 
        user.save()
        value = "Activated" if user.is_active else "Deactivated"
        messages.info(request, "User {}".format(value))

        mail = DbSendMail("UAAA")

        site = RequestSite(request)
        mail_context = dict(name=user.get_full_name(),
                            status=value,
                            login_url=reverse_url_with_hostname(site, "owj_login"))
        mail.sendmail([user.email], mail_context)
        redirect_url = reverse("admin:main_user_change", args=(user.id,))
        return redirect(redirect_url)


    def get_urls(self):
        urls = super(OwjUserAdmin, self).get_urls()

        list_urls = patterns('',
                url(r'^activate/(?P<userpk>\d+)/$', self.admin_site.admin_view(self.activate_user_view), {'active':True}, name="admin_activate_user"),
                url(r'^deactivate/(?P<userpk>\d+)/$', self.admin_site.admin_view(self.activate_user_view), {'active':False}, name="admin_deactivate_user"),
            )
        return list_urls + urls


#registration to administrator site
admin_site.register(User, OwjUserAdmin)
admin_site.register(Page, PageAdmin)
admin_site.register(MailTemplate, MailTemplateAdmin)

#registration to developers site
admin.site.register(User, OwjUserAdmin)
admin.site.register(Permission)