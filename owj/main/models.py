from django.db import models

# Create your models here.
from django.contrib.auth.models import AbstractUser, Group
from django.contrib.auth.models import UserManager as AuthUserManager

class Role(models.Model):
    name = models.CharField(max_length=10)
    group = models.ForeignKey(Group)

    def __unicode__(self):
        return self.name


class UserManager(AuthUserManager):

    def create_superuser(self, username, email, password, **extra_fields):
        """
        creates a super user with role superadmin.
        this will create a superuser group and role if not present
        """
        group, is_created = Group.objects.get_or_create(name="SuperAdmin")
        role, is_created = Role.objects.get_or_create(group=group, name="SuperAdmin")
        if not extra_fields.has_key("role"): extra_fields["role"] = role
        return AuthUserManager.create_superuser(self, username, email, password, **extra_fields)

    def editors(self, *args, **kwargs):
        role = Role.objects.get(name="Editor")
        return self.get_queryset().filter(role=role, *args, **kwargs)
    
    def admins(self, *args, **kwargs):
        role = Role.objects.get(name="Admin")
        return self.get_queryset().filter(role=role, *args, **kwargs)
    
    def admin_and_editors(self, *args, **kwargs):
        roles = Role.objects.filter(name__in = ["Admin", "Editor"])
        return self.get_queryset().filter(role__in = roles, *args, **kwargs)
    


class User(AbstractUser):
    """
    User table with aditional fields.
    """
    role = models.ForeignKey(Role, help_text="Users Role")
    display_name = models.CharField(max_length=30, blank=True)
    affiliation = models.CharField(max_length=256, blank=True)
    about_me = models.TextField(max_length=2048, blank=True)

    objects = UserManager()
    
    class Meta:
        permissions = (
            ("can_activate_user", "can activate user"),
        )

    def __unicode__(self):
        return self.get_full_name() or str(self.email)
    
    def get_full_name(self):
        return AbstractUser.get_full_name(self) or self.username
    
    def get_display_name(self):
        return self.display_name or self.get_full_name()
    
    def can_view_editor_menu(self):
        """
        Only admin and editor can view the Editor menu
        """
        return self.role.name.lower() in ["editor", "admin"]


