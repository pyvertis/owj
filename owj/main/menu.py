from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from cms.menu_bases import CMSAttachMenu
from menus.base import Menu, NavigationNode
from menus.menu_pool import menu_pool

class AuthorMenu(CMSAttachMenu):
    name = _("Author Menu")
    
    def get_nodes(self, request):
        nodes =[]
        
#         nodes.append(NavigationNode("Author Instructions", 
#                                     reverse("author"), 
#                                     "author"))
        nodes.append(NavigationNode("My Submissions", 
                                    reverse("author_submissions"), 
                                    "author_submissions"))
        nodes.append(NavigationNode("Submit Article", 
                                    reverse("create_article"), 
                                    "author_submissions"))
        return nodes


class ReviewerMenu(CMSAttachMenu):
    name = _("Reviewer Menu")
    
    def get_nodes(self, request):
        nodes =[]
        
#         nodes.append(NavigationNode("Author Instructions", 
#                                     reverse("author"), 
#                                     "author"))
        nodes.append(NavigationNode("My Submissions", 
                                    reverse("author_submissions"), 
                                    "author_submissions"))
        nodes.append(NavigationNode("Submit Article", 
                                    reverse("create_article"), 
                                    "author_submissions"))
        return nodes




menu_pool.register_menu(AuthorMenu)
menu_pool.register_menu(ReviewerMenu)