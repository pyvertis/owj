from rest_framework import filters
from articles.models import ArticleReviewers, ReviewerReview, Issue
import datetime
from django.db.models import Q

REVIEWER_DATE_FILTERS = (('week', 'This Week'), ('month', 'This Month'), ('year', 'This Year'))
REVIEWER_FILTERS = (('notyetreviewed', 'Not Yet Reviewed'),)
ISSUE_FILTERS = (('false', 'UnPublished'), ('true', 'Published'))


class UserRoleFilter(filters.BaseFilterBackend):
    """
    Filter the users by role. id or role name both can be accepted as role value.
    """
    def filter_queryset(self, request, queryset, view):
        role = request.QUERY_PARAMS.has_key("role") and request.QUERY_PARAMS["role"]
        if role:
            try:
                role = int(role)
                queryset = queryset.filter(role__id=role)
            except ValueError:
                queryset = queryset.filter(role__name__icontains=role)

        return queryset


class SearchUserFilter(filters.BaseFilterBackend):
    """
    Filter to search user by firstname, lastname or email
    """
    def filter_queryset(self, request, queryset, view):
        query = request.QUERY_PARAMS.has_key("q") and request.QUERY_PARAMS["q"]
        if query:
            firstnameQ = Q(first_name__startswith=query)
            lastnameQ = Q(last_name__startswith=query)
            emailQ = Q(email__icontains=query)
            queryset = queryset.filter(firstnameQ | lastnameQ | emailQ)

        return queryset


class ArticleReviewsFilter(filters.BaseFilterBackend):
    """
    Filter for filtering reviews based on article
    """
    def filter_queryset(self, request, queryset, view):
        articleid = request.QUERY_PARAMS.has_key("article") and request.QUERY_PARAMS["article"]
        if articleid:
            queryset = queryset.filter(article=articleid)
        return queryset


class UsersOnlyReviews(ArticleReviewsFilter):
    """
    filters logged in users review only
    """
    def filter_queryset(self, request, queryset, view):
        queryset = ArticleReviewsFilter.filter_queryset(self, request, queryset, view)
        if request.user.role.name.lower() == "reviewer":
            queryset = queryset.filter(reviewer=request.user)
        return queryset


class AuthorArticleReviewsFilter(filters.BaseFilterBackend):
    """
    Authors should be able to view reviews only on their articles.
    They can view reviews only when 'author_can_view_reviews' flag is set 
    on an article.
    """
    def filter_queryset(self, request, queryset, view):
        if request.user.role.name.lower() == "author":
            queryset = queryset.filter(article__author=request.user,
                            article__author_can_view_reviews=True)
        return queryset


class IsReviewerFilterBackend(filters.BaseFilterBackend):
    """
    Filter that only allows users to see their own objects.
    """
    def filter_queryset(self, request, queryset, view):
        myarticles = ArticleReviewers.objects.filter(reviewer=request.user).values_list("article")
        return queryset.filter(id__in=myarticles)


class NotReviewedFilterBackend(filters.BaseFilterBackend):
    """
    Filter that only returns articles which is not reviewed by current reviewer.
    """
    def filter_queryset(self, request, queryset, view):
        if request.QUERY_PARAMS.has_key("status"):
            query = request.QUERY_PARAMS["status"]
            valid_status = [i[0] for i in REVIEWER_FILTERS]
            if query in valid_status:
                myReviewedArticles = ReviewerReview.objects.filter(reviewer=request.user).values_list("article")
                return queryset.exclude(id__in=myReviewedArticles)

        return queryset


class IsOwnerFilterBackend(filters.BaseFilterBackend):
    """
    Filter that only allows users to see their own objects.
    """
    def filter_queryset(self, request, queryset, view):
        return queryset.filter(author=request.user)


class BiddingPoolFilter(filters.BaseFilterBackend):
    """
    Filter items which are in bidding pool
    """
    def filter_queryset(self, request, queryset, view):
        if request.QUERY_PARAMS.has_key("bidding") and request.QUERY_PARAMS["bidding"] == "true":
            return queryset.filter(in_bidding_pool=True)
        else: return queryset


class ArticleCreatedFilter(filters.BaseFilterBackend):
    """
    Filter items based on created date.
    """
    def filter_queryset(self, request, queryset, view):
        if request.QUERY_PARAMS.has_key("date"):
            query = request.QUERY_PARAMS["date"]
            valid_date = [i[0] for i in REVIEWER_DATE_FILTERS]
            if query in valid_date:
                if query == 'week':
                    last_monday = __get_monday()
                    return queryset.filter(created__gte=last_monday)
                if query == 'month':
                    first_day_of_month = __get_first_day_of_month()
                    return queryset.filter(created__gte=first_day_of_month)
                if query == 'year':
                    first_day_of_year = __get_first_day_of_year()
                    return queryset.filter(created__gte=first_day_of_year)

            return queryset
        else: return queryset


class ArticleNotInIssueFilter(filters.BaseFilterBackend):
    """
    Filters articles which are not in any issue.
    """
    def filter_queryset(self, request, queryset, view):
        if request.QUERY_PARAMS.has_key("notinissue"):
            query = request.QUERY_PARAMS["notinissue"]
            if query == 'true':
                __artilces = Issue.objects.distinct().values_list("articles", flat=True)
                articles_in_issues = [ i for i in __artilces if i]
                return queryset.exclude(id__in=articles_in_issues)

        return queryset


class IssuePublishedUnpublishedFilter(filters.BaseFilterBackend):
    """
    Filter for filtering issues based on status (uplished, unpublished)
    """
    def filter_queryset(self, request, queryset, view):
        is_published = request.QUERY_PARAMS.has_key("published") and request.QUERY_PARAMS["published"]
        if is_published == 'true':
            queryset = queryset.filter(is_published=True)
        elif is_published == 'false':
            queryset = queryset.filter(is_published=False)

        return queryset


def __get_monday(date=None):
    '''
    gets the first day of current week.
    '''
    today = date or datetime.datetime.today()
    factor = 1 - today.isoweekday()
    return today + datetime.timedelta(days=factor)

def __get_first_day_of_month(date=None):
    '''
    gets the first day of current month.
    '''
    today = date or datetime.datetime.today()
    return date(today.year, today.month, 1)

def __get_first_day_of_year(date=None):
    '''
    gets the first day of current month.
    '''
    today = date or datetime.datetime.today()
    return date(today.year, 1, 1)
