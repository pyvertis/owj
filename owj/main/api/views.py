"""
To get articles which are in bidding pool
/api/v1/articles/?bidding=true

To filter articles with status
/api/v1/articles/?status=valid_status_value
"""

from articles.models import Article, ArticleReviewers, ArticleReview, \
    ReviewerReview, EditorReview, Volume, Issue, InvitedUsers
from articles.signals import review_written_signal, \
    publish_issue_articles_signal
from dbmail.mail import DbSendMail
from django.contrib.auth import get_user_model
from django.contrib.sites.models import RequestSite
from main.api.filters import UserRoleFilter, BiddingPoolFilter, \
    ArticleCreatedFilter, IsOwnerFilterBackend, IsReviewerFilterBackend, \
    NotReviewedFilterBackend, ArticleReviewsFilter, AuthorArticleReviewsFilter, \
    UsersOnlyReviews, REVIEWER_FILTERS, REVIEWER_DATE_FILTERS, \
    ArticleNotInIssueFilter, IssuePublishedUnpublishedFilter, ISSUE_FILTERS, \
    SearchUserFilter
from main.api.permissions import ArticleCreatePermission, \
    ReviewArticlePermissions, IsEditorPermissions
from main.api.serializers import CurrentUserSerializer, ListUserSerializer, \
    ArticleSerializer, ReviewerArticleSerializer, ReviewSerializerBase, \
    ReviewersReviewSerializer, EditorsReviewSerializer, IssueSerializer, \
    AnonymousUserSerializer, InviteUserSerializer, MostPopularArticlesSerializer, \
    OwjPaginationSerializer
from owj.utils import reverse_url_with_hostname
from rest_framework import viewsets
from rest_framework.generics import ListAPIView, get_object_or_404, \
    ListCreateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
import datetime
import random
import string
import logging
from main.views import Articles

User = get_user_model()
logger = logging.getLogger("owj")

class CurrentUserView(APIView):
    """
    Retrieve, update current logged in user.
    """
    def get_object(self, request):
        return request.user

    def get(self, request, format=None):
        user = self.get_object(request)
        if user.is_authenticated():
            serializer = CurrentUserSerializer(user)
        else:
            serializer = AnonymousUserSerializer(user)
        return Response(serializer.data)


class StatusListView(APIView):

    def get(self, request, *args, **kwargs):

        return Response(Article.ARTICLE_STATUS)


class ReviewerStatusListView(APIView):

    def get(self, request, *args, **kwargs):

        return Response(REVIEWER_FILTERS)

class IssueStatusListView(APIView):

    def get(self, request, *args, **kwargs):

        return Response(ISSUE_FILTERS)


class DateListView(APIView):
    def get(self, request, *args, **kwargs):

        return Response(REVIEWER_DATE_FILTERS)


class UsersListView(ListAPIView):
    """
    List all the users. 
    role filter allows getting list of users by role
    """
    model = User
    serializer_class = ListUserSerializer
    filter_backends = (UserRoleFilter, SearchUserFilter)

    def get_queryset(self):
        users = User.objects.filter(is_active=True)
#         users = users.filter(first_name__startswith=)
        return users


class ArticleViewSet(viewsets.ModelViewSet):
    model = Article
    filter_backends = (BiddingPoolFilter, ArticleCreatedFilter, ArticleNotInIssueFilter)
    serializer_class = ArticleSerializer
    paginate_by = 10
    pagination_serializer_class = OwjPaginationSerializer

    def get_queryset(self):
        queryset = viewsets.ModelViewSet.get_queryset(self)
        if self.request.QUERY_PARAMS.has_key("status") and self.request.QUERY_PARAMS["status"]:
            query = list(set(self.request.QUERY_PARAMS.getlist('status')) & 
                         set([i[0] for i in self.model.ARTICLE_STATUS]))
            if query:
                return queryset.filter(status__in=query)

        return queryset


class AuthorViewSet(ArticleViewSet):
    filter_backends = (IsOwnerFilterBackend,)
    permission_classes = (ArticleCreatePermission,)


class ReviewerViewSet(ArticleViewSet):

    filter_backends = (IsReviewerFilterBackend, NotReviewedFilterBackend)
    serializer_class = ReviewerArticleSerializer

    def get_queryset(self):
        """
        returns articles which are assigned to logged in reviewer 
        """
        queryset = ArticleViewSet.get_queryset(self)
        articles = ArticleReviewers.objects.filter(reviewer=self.request.user).values_list("article")
        queryset.filter(id__in=articles)
        return queryset


class EditorViewSet(ArticleViewSet):
    permission_classes = (ArticleCreatePermission,)


class ReviewsViewSet(viewsets.ModelViewSet):
    """
    Operations on Artcile Reviews
    """
    model = ArticleReview
    permission_classes = (ReviewArticlePermissions,)
    serializer_class = ReviewSerializerBase
    filter_backends = (ArticleReviewsFilter, AuthorArticleReviewsFilter)

    def pre_save(self, obj):
        """
        As only logged in user can write review. automatically add the 
        current user as reviewer
        """
        obj.reviewer = self.request.user
        viewsets.ModelViewSet.pre_save(self, obj)


class ReviewerReviewsViewSet(ReviewsViewSet):
    """
    Reviews for and by Reviewers
    """
    model = ReviewerReview
    serializer_class = ReviewersReviewSerializer
    filter_backends = (UsersOnlyReviews,)

    def post_save(self, obj, created=False):
        """
        Sends a signal when item is saved.
        """
        review_written_signal.send(sender=self.__class__, review_obj=obj)
        viewsets.ModelViewSet.post_save(self, obj, created=created)


class EditorReviewsViewSet(ReviewsViewSet):
    """
    Allows operations on Editor Reviews
    """
    model = EditorReview
    permission_classes = ReviewsViewSet.permission_classes + (IsEditorPermissions,)
    serializer_class = EditorsReviewSerializer

    def _get_mail_context(self, obj):
        return {"user": obj.article.author.get_full_name(),
                "article_title": obj.article.title
            }

    def _accept_article(self, obj):
        """
        set status to accept and send mail
        """
        obj.article.status = "accepted"
        obj.article.status_date = datetime.datetime.now()
        obj.article.author_can_view_reviews = True
        obj.article.save()

        mail = DbSendMail("ACMA")
        mail_context = self._get_mail_context(obj)
        recipient_list = [obj.article.author.email]  # list
        mail.sendmail(recipient_list, mail_context)


    def _revise_article(self, obj):
        """
        set status to revise and send mail
        """
        obj.article.status = "reopened"
        obj.article.status_date = datetime.datetime.now()
        obj.article.author_can_view_reviews = True
        obj.article.save()

        mail = DbSendMail("ARVM")
        mail_context = self._get_mail_context(obj)
        recipient_list = [obj.article.author.email]  # list
        mail.sendmail(recipient_list, mail_context)

    def _reject_article(self, obj):
        """
        set status to reject and send mail
        """
        obj.article.status = "rejected"
        obj.article.status_date = datetime.datetime.now()
        obj.article.author_can_view_reviews = True
        obj.article.save()

        mail = DbSendMail("ARMA")
        mail_context = self._get_mail_context(obj)
        recipient_list = [obj.article.author.email]  # list
        mail.sendmail(recipient_list, mail_context)

    def post_save(self, obj, created=False):
        """
        When Editor submits a review that will go as final decision.
        based on the recommentdation type, mail will be sent and workflow will change
        """
        if created:
            post_action = {"accepted": self._accept_article,
             "revise": self._revise_article,
             "rejected": self._reject_article
             }
            post_action[obj.recommendation](obj)


        viewsets.ModelViewSet.post_save(self, obj, created=created)


class ListArticleReviewers(APIView):
    """
    View to list all users in the system.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    def get(self, request, format=None, **kwargs):
        """
        Return a list of all users.
        """
        article = get_object_or_404(Article.objects.all(), id=kwargs["id"])
        suggested_reviewers = article.ext_reviewers.all().values("first_name", "last_name",
                                                "affiliation", "email")
        assigned_reviewers = article.articlereviewers_set.all().values("reviewer__first_name",
            "reviewer__last_name", "reviewer__affiliation", "reviewer__email")


        return Response({"suggested_reviewers":suggested_reviewers,
                         "assigned_reviewers":assigned_reviewers
                         })


    def post(self, request, *args, **kwargs):
        article = get_object_or_404(Article.objects.all(), id=kwargs["id"])
        user_ids = request.DATA["users"]
        already_addded_users = article.articlereviewers_set.all().values_list("reviewer")

        # if there is any user which is already added to article, exclude will remove those users
        users = User.objects.filter(id__in=user_ids).exclude(id__in=already_addded_users)

        # users
        for user in users:
            art_reviewer, is_created = ArticleReviewers.objects.get_or_create(article=article, reviewer=user)
            mail = DbSendMail("MREA")
            mail_context = {"name" : user.get_full_name(),
                            "article_title": article.title
                            }
            recipient_list = [user.email]
            mail.sendmail(recipient_list, mail_context)
            # send a mail to reviewer

        return Response("Ok")


class VolumeViewSet(viewsets.ModelViewSet):
    '''
    View set to retrive all volumes.
    '''
    model = Volume
    permission_classes = (IsEditorPermissions,)


class IssueViewSet(viewsets.ModelViewSet):
    '''
    View set to create and retrive all issues.
    '''
    model = Issue
    permission_classes = (IsEditorPermissions,)
    serializer_class = IssueSerializer
    filter_backends = (IssuePublishedUnpublishedFilter,)

    def pre_save(self, obj):
        """
        As only logged in user can create issue. automatically add the 
        current user as editor.
        """
        obj.editor = self.request.user
        viewsets.ModelViewSet.pre_save(self, obj)

    def post_save(self, obj, created):
        # only publish issue articles if query string was passed.
        if self.request.QUERY_PARAMS.has_key("trigger_article_publish") and \
                self.request.QUERY_PARAMS["trigger_article_publish"] == 'true':
            publish_issue_articles_signal.send(sender=self.__class__, issue=obj)


class ListVolIssuesView(APIView):
    """
    List issues for a given Vol.
    """
    def get(self, request, *args, **kwargs):
        vol_id = kwargs["id"]
        volume = get_object_or_404(Volume.objects.all(), id=vol_id)
        issues = []
        for issue in volume.issue_set.all():
            issues.append({
                "id": issue.id,
                "title": issue.title,
                "issue_number": issue.issue_number
            });


        return Response(issues)


class InviteUserView(ListCreateAPIView):
    """
    Api to allow creating invites and listing invites
    """
    model = InvitedUsers
    serializer_class = InviteUserSerializer

    def pre_save(self, obj):
        """
        Auto generate the invotation code
        """
        while True:
            invitation_code = ''.join(random.sample(string.letters + string.digits, 10))
            if not self.model.objects.filter(invitation_code=invitation_code).exists():
                break
        obj.invitation_code = invitation_code
        return ListCreateAPIView.pre_save(self, obj)

    def post_save(self, obj, created=False):
        """
        Send invitation to user 
        """
        if created:
            site = RequestSite(self.request)
            invitation_url = reverse_url_with_hostname(site, "reviwer_invitation")
            invitation_url = "{}?code={}".format(invitation_url, obj.invitation_code)
            mail = DbSendMail("IUMT")
            mail_context = {"user": obj.get_display_name(),
                            "message": obj.message,
                            "link": invitation_url
                            }
            mail.sendmail([obj.email], mail_context)
            logger.info("a user has been invited. email: {}".format(obj.email))

        return ListCreateAPIView.post_save(self, obj, created=created)




class ListMostPopularArticles(ListAPIView):
    model = Article
    serializer_class = MostPopularArticlesSerializer
    
    def filter_queryset(self, queryset):
        queryset = ListAPIView.filter_queryset(self, queryset)
        return queryset.order_by("file_download_count", "publish_date")[:20]
        
        
         
    
    
