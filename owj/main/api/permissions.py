from rest_framework.permissions import BasePermission, IsAuthenticatedOrReadOnly,\
    SAFE_METHODS


class ReviewArticlePermissions(BasePermission):
    """
    checks it user has permissions to write review for article
    """
    def _has_write_permission(self, request, view):
        has_perm = True
        if request.method.lower() in ["post", "put", "patch", "delete"]:
            has_perm = request.user.has_perm("articles.add_articlereview")
        return has_perm    
    
    def has_permission(self, request, view):
        return self._has_write_permission(request, view)


class IsEditorPermissions(IsAuthenticatedOrReadOnly):
    """
    checks it user has permissions to write review for article
    """
    def has_permission(self, request, view):
        return (request.method in SAFE_METHODS or 
            request.user and 
            request.user.is_authenticated() and 
            request.user.role.name.lower() in ["editor", "admin"])
    

class ArticleCreatePermission(BasePermission):

    def has_permission(self, request, view):
        return True
