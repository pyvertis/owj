from django.contrib.auth import get_user_model
from rest_framework import serializers, filters
from articles.models import Issue, ArticleReview, Article, ReviewerReview, \
    EditorReview, InvitedUsers
from django.contrib.auth.models import AnonymousUser
from rest_framework.serializers import Serializer
from rest_framework.pagination import PaginationSerializer, NextPageField, \
    PreviousPageField

User = get_user_model()


class OwjNextPageField(NextPageField):
    """
    field that return next page number, not the url
    """
    def to_native(self, value):
        if not value.has_next():
            return None
        return value.next_page_number()


class OwjPreviousPageField(PreviousPageField):
    """
    field that return next page number, not the url
    """
    def to_native(self, value):
        if not value.has_previous():
            return None
        return value.previous_page_number()

class OwjPaginationSerializer(PaginationSerializer):
    next = OwjNextPageField(source='*')
    previous = OwjPreviousPageField(source='*')


class AnonymousUserSerializer(Serializer):
    """
    Serialiers for logged out user.
    """
    permissions = serializers.SerializerMethodField('user_permissions')
    is_authenticated = serializers.CharField(source='is_authenticated')

    class Meta:
        model = AnonymousUser
        fields = ("permissions", "is_authenticated")

    def user_permissions(self, obj):
        return obj.get_all_permissions()


class CurrentUserSerializer(serializers.ModelSerializer):
    """
    for data to be sent for current logged in user
    """
    full_name = serializers.CharField(source='get_full_name')
    permissions = serializers.SerializerMethodField('user_permissions')
    role = serializers.SerializerMethodField('get_role')
    is_authenticated = serializers.CharField(source='is_authenticated')

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'full_name', 'is_superuser', 'email', 'is_staff', 'role',
                  "display_name", "affiliation", "about_me", "permissions", "is_authenticated")

    def user_permissions(self, obj):
        return obj.get_all_permissions()

    def get_role(self, obj):
        return {
            "id":obj.role.id,
            "name":obj.role.name.lower()
        }


class ListUserSerializer(CurrentUserSerializer):

    class Meta(CurrentUserSerializer.Meta):
        fields = ('id', 'first_name', 'last_name', 'full_name', 'email', 'role',
                  "display_name", "affiliation", "about_me")


class IssueSerializer(serializers.ModelSerializer):
    '''
    Serializer for issue.
    '''
    articles = serializers.SerializerMethodField('_get_issue_articles')
    articles_ids = serializers.PrimaryKeyRelatedField(many=True, source='articles')
    class Meta:
        model = Issue
        read_only_fields = ("editor", "created")
        fields = ("id", "title", "volume", "issue_number",
                  "is_published", "publish_date",
                  "articles", "articles_ids")

    def _get_issue_articles(self, obj):
        """
        Gets the article for current issue.
        """
        issue_articles = obj.articles.all()
        articles = []
        if issue_articles:
            for issue_article in issue_articles:
                article = {
                    "id": issue_article.id,
                    "title": issue_article.title,
                    "author": issue_article.author.get_full_name(),
                    "created": issue_article.created,
                    "status": issue_article.status,
                    }
                articles.append(article)
        else:
            articles = []

        return articles


class ArticleSerializer(serializers.ModelSerializer):
    ext_authors = serializers.SerializerMethodField('_get_ext_authors')
    ext_reviewers = serializers.SerializerMethodField('_get_ext_reviewers')
    abs_url = serializers.Field(source='get_absolute_url')
    key_words = serializers.SerializerMethodField('_get_key_words')
    can_author_pay = serializers.Field(source='can_author_pay')


    class Meta:
        model = Article
        fields = ("id", "title", "abstract", "status",
                  "paid", "in_bidding_pool", "status_date", "publish_date",
                  "modified", "created", "ext_authors", "ext_reviewers", "abs_url",
                  "key_words", "pdf", "can_author_pay")
        depth = 1

    def _get_ext_authors(self, obj):
        """
        Gets the Ext authors for article
        """
        ext_authors = obj.ext_authors.all()
        authors = []
        if ext_authors:
            for ext_author in ext_authors:
                author = {
                    "first_name": ext_author.first_name,
                    "last_name": ext_author.last_name,
                    "email": ext_author.email,
                    "affiliation": ext_author.affiliation,
                    "is_author": ext_author.is_author,
                    }
                authors.append(author)
        else:
            authors = []

        return authors

    def _get_ext_reviewers(self, obj):
        """
        Gets the Ext authors for article
        """
        ext_reviewers = obj.ext_reviewers.all()
        reviewers = []
        if ext_reviewers:
            for ext_reviewer in ext_reviewers:
                reviewer = {
                    "first_name": ext_reviewer.first_name,
                    "last_name": ext_reviewer.last_name,
                    "email": ext_reviewer.email,
                    "affiliation": ext_reviewer.affiliation,
                    }
                reviewers.append(reviewer)
        else:
            reviewers = []

        return reviewers


    def _get_key_words(self, obj):
        """
        Gets the key words for article in list.
        """
        return obj.key_words.all()



class ReviewerArticleSerializer(ArticleSerializer):
#     article = serializers.SerializerMethodField('_get_article')
    reviews = serializers.SerializerMethodField('_get_article_reviews')

    class Meta(ArticleSerializer.Meta):
        fields = ArticleSerializer.Meta.fields + ('reviews',)
        depth = 2

    def _get_article_reviews(self, obj):
        """
        Gets the reviews for article
        """
        request = self.context["request"]
        obj_reviews = ReviewerReview.objects.filter(article=obj, reviewer=request.user)
        reviews = []
        if obj_reviews:
            for obj_review in obj_reviews:

                review = {
                    "review": obj_review.comment,
                    "review_status": obj_review.recommendation,
                    "created": obj_review.created,
                }
                reviews.append(review)

        else:
            reviews = []

        return reviews



class ReviewSerializerBase(serializers.ModelSerializer):
    reviewer = serializers.SerializerMethodField('get_reviewer_info')

    class Meta:
        model = ArticleReview
        fields = ("id", "article", "comment", "recommendation", "created", "reviewer")
        read_only_fields = ("created",)

    def get_reviewer_info(self, obj):
        if hasattr(obj, "reviewerreview"):
            data = {"name": obj.reviewerreview.reviewer.get_full_name(),
                    "role": obj.reviewerreview.reviewer.role
                }
        elif hasattr(obj, "editorreview"):
            data = {"name": obj.editorreview.reviewer.get_full_name(),
                "role": obj.editorreview.reviewer.role
            }
        else:
            data = {}
        return data


class ReviewersReviewSerializer(ReviewSerializerBase):
    class Meta:
        model = ReviewerReview


class EditorsReviewSerializer(ReviewSerializerBase):
    class Meta:
        model = EditorReview


class InviteUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = InvitedUsers
        read_only_fields = ('invitation_code',)


class MostPopularArticlesSerializer(serializers.ModelSerializer):
    author = serializers.SerializerMethodField('get_author_name')
    class Meta:
        model = Article

    def get_author_name(self, obj):
        return obj.author.get_display_name()


